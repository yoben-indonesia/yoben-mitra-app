import React from 'react';
import { StatusBar } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { enableScreens } from 'react-native-screens';

import { store } from 'stores';
import { Alert } from 'components';
import AppNavigator from './AppNav';

enableScreens();

const AppNav = () => {
  const [initialRouteName, setInitialRouteName] = React.useState('Login');
  const token = store.getState().mitra?.data?.token;
  const status = store.getState().mitra?.data?.status;

  React.useEffect(() => {
    token && status === 'active' && setInitialRouteName('Dashboard');
  }, [])

  return (
    <>
      <StatusBar barStyle='dark-content' />
      <SafeAreaProvider>
        <AppNavigator initialRouteName={initialRouteName} />
      </SafeAreaProvider>

      <Alert />
    </>
  );
};

export default AppNav;
