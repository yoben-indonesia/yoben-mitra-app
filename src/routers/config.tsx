import {
  Login, Otp, CreatePassword, CustPreferences, ForgorPassword,
  Balance, InfoBalance, ServiceScore, TopUp, Notifications,
  OrderDetails, Payment,
  Setting, TnC, ChangePwd, Feedback,
  Camera, Success,
} from 'screens';
import { navScreenSlider } from 'helpers';

import DashboardBottomTab from './routersComponents/DashboardBottomTab';
import CustomHeader from './routersComponents/CustomHeader';

const screenRouters: any = [
  {
    name: 'Login',
    component: Login,
    options: {
      header: () => null,
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    }
  },
  {
    name: 'Otp',
    component: Otp,
    options: {
      header: () => null,
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    }
  },
  {
    name: 'CreatePassword',
    component: CreatePassword,
    options: {
      header: () => null,
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    }
  },
  {
    name: 'ForgorPassword',
    component: ForgorPassword,
    options: {
      header: () => null,
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    }
  },
  {
    name: 'CustPreferences',
    component: CustPreferences,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'CustPreferences'),
      headerShown: false,
      cardStyle: {
        backgroundColor: 'white',
      },
      cardStyleInterpolator: navScreenSlider,
    })
  },
  //
  {
    name: 'Dashboard',
    component: DashboardBottomTab,
    options: dashboardOptions
  },
  {
    name: 'Balance',
    component: Balance,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'Balance'),
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    })
  },
  {
    name: 'InfoBalance',
    component: InfoBalance,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'InfoBalance'),
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    })
  },
  {
    name: 'ServiceScore',
    component: ServiceScore,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'ServiceScore'),
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    })
  },
  {
    name: 'TopUp',
    component: TopUp,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'TopUp'),
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    })
  },
  //
  {
    name: 'OrderDetails',
    component: OrderDetails,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'OrderDetails'),
      cardStyle: {
        backgroundColor: 'white',
      },
      cardStyleInterpolator: navScreenSlider,
    })
  },
  {
    name: 'Payment',
    component: Payment,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'Payment'),
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    })
  },
  //
  {
    name: 'Setting',
    component: Setting,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'Setting'),
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    })
  },
  {
    name: 'Notifications',
    component: Notifications,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'Notifications'),
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    })
  },
  {
    name: 'TnC',
    component: TnC,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'TnC'),
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    })
  },
  {
    name: 'ChangePwd',
    component: ChangePwd,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'ChangePwd'),
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    })
  },
  {
    name: 'Feedback',
    component: Feedback,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'Ulasan'),
      cardStyleInterpolator: navScreenSlider,
      cardStyle: {
        backgroundColor: 'white',
      },
    })
  },
  //
  {
    name: 'Camera',
    component: Camera,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'Camera'),
      cardStyleInterpolator: navScreenSlider,
    })
  },
  {
    name: 'Success',
    component: Success,
    options: ({ navigation }) => ({
      header: () => CustomHeader(navigation, 'Success'),
      cardStyle: {
        backgroundColor: 'white',
      },
      cardStyleInterpolator: navScreenSlider,
    })
  },
]

function dashboardOptions({ navigation, route }) {
  const routeName = route.state ? route.state.routes[route.state.index].name : 'Home';

  return {
    header: () => CustomHeader(navigation, routeName),
    cardStyleInterpolator: navScreenSlider,
    cardStyle: {
      backgroundColor: 'white',
    },
  };
}

export default screenRouters;