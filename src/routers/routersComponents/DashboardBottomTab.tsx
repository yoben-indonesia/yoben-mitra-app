import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { IcHome, IcOrder, IcProfile } from 'svg';
import { BoxShadow } from 'react-native-shadow';

import { Text } from 'components';
import { normalize } from 'helpers';
import { colors, themes } from 'consts';
import { Home, MyProfile } from 'screens';
import OrderTopTabs from './OrderTopTabs';

const BottomTab = createBottomTabNavigator();

const CustomBottomTab = ({ state, descriptors, navigation }) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options;
  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <BoxShadow setting={themes.rnShadowConfig(70)}>
      <View style={Style.customTabContainer}>
        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
                ? options.title
                : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          const iconProps = {
            opacity: isFocused ? 1 : 0.2,
            style: {
              marginBottom: normalize(3)
            }
          }
          let icon;
          if (label === 'Home') icon = <IcHome {...iconProps} />
          else if (label === 'Order') icon = <IcOrder {...iconProps} />
          if (label === 'Profile') icon = <IcProfile {...iconProps} />

          return (
            <TouchableOpacity
              accessibilityRole='button'
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              activeOpacity={1}
              key={route.name}
              style={Style.btnContainer}
            >
              {icon}
              <Text
                weight='regular'
                style={{ color: isFocused ? colors.yobenBlue : 'rgba(45, 104, 140, 0.2)' }}
                text={label}
              />
            </TouchableOpacity>
          );
        })}
      </View>
    </BoxShadow>
  )
}

function BottomTabNav() {
  return (
    <BottomTab.Navigator
      tabBar={props => <CustomBottomTab {...props} />}
      initialRouteName='Home'
    >
      <BottomTab.Screen name='Home' component={Home} />
      <BottomTab.Screen name='Order' component={OrderTopTabs} />
      <BottomTab.Screen name='Profile' component={MyProfile} />
    </BottomTab.Navigator>
  );
}

const Style = StyleSheet.create({
  customTabContainer: {
    flexDirection: 'row',
    shadowOpacity: 0.5,
    shadowRadius: 10,
    shadowOffset: {
      height: normalize(-5),
      width: 0,
    },
    height: normalize(70),
    shadowColor: 'rgba(45, 104, 140, 0.1)',
  },
  btnContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: normalize(20),
    backgroundColor: 'white'
  }
})

export default BottomTabNav;