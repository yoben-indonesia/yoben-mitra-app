import React from 'react';
import { Platform, View, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { BoxShadow } from 'react-native-shadow';

import { normalize } from 'helpers';
import { Button, Text } from 'components';
import { LogoYoben, IcNotif } from 'svg';
import { colors, themes, screenWidth } from 'consts';

const CustomHeader = (navigation, routeName) => {
  const content = () => (
    <View style={Style(routeName).headerContainer}>

      <View style={Style().leftHeader}>
        {LeftHeader(navigation, routeName)}
      </View>

      <View style={Style().headerTitle}>
        <Text size='m' color='black' text={headerTitle(routeName)} />
      </View>

      <View style={Style().rightHeader}>
        {RightHeader(navigation, routeName)}
      </View>
    </View>
  )

  if (routeName === 'Camera' || routeName === 'Order') {
    return content();
  }

  return (
    <BoxShadow setting={themes.rnShadowConfig(60)}>
      {content()}
    </BoxShadow>
  );
};

const headerTitle = (routeName: string) => {
  switch (routeName) {
    case 'Balance':
      return 'Saldo Anda';
    case 'InfoBalance':
      return 'Info Tentang Saldo';
    case 'ServiceScore':
      return 'Nilai Pelayanan Anda';
    case 'TopUp':
      return 'Top Up';
    case 'Order':
      return 'Order';
    case 'OrderDetails':
      return 'Detail Order';
    case 'Payment':
      return 'Pembayaran';
    case 'Profile':
      return 'Profile Saya';
    case 'Setting':
      return 'Pengaturan';
    case 'Notifications':
      return 'Notifikasi';
    case 'TnC':
      return 'Syarat & Ketentuan';
    case 'ChangePwd':
      return 'Ganti Password';
    case 'CustPreferences':
      return 'Preferensi Customer';
    case 'Success':
      return 'Selesai';
    default: return '';
  }
};

const LeftHeader = (navigation: any, routeName: string) => {
  function onPressBack() {
    navigation.pop()
  }

  switch (routeName) {
    case 'Home':
      return <LogoYoben style={{ marginLeft: normalize(-10) }} />;
    case 'Order':
    case 'Profile':
    case 'Success':
      return null;
    default: return <Icon name='chevron-back' size={normalize(28)} color={routeName === 'Camera' ? colors.white : colors.greyText} onPress={onPressBack} />;
  }
};

function onPressNavigate(routeName: string, navigation: any) {
  return () => navigation.navigate(routeName)
}

const RightHeader = (navigation: any, routeName: string) => {
  switch (routeName) {
    // case 'Home':
    //   return NotifBtn(navigation);
    case 'Profile':
      return <Icon name="settings-sharp" size={normalize(18)} color={colors.greyText} onPress={onPressNavigate('Setting', navigation)} />;
    case 'Balance':
      return <Button type='text' text='Info' onPress={onPressNavigate('InfoBalance', navigation)} />;
    case 'Camera':
      return <Icon name='image-sharp' size={normalize(26)} color={colors.white} />;
    default: return null;
  }
};
const notif = 2;
const NotifBtn = (navigation: any) => (
  <TouchableOpacity style={Style().notifBtnContainer} onPress={onPressNavigate('Notifications', navigation)} activeOpacity={1}>
    <IcNotif />
    <View style={Style().notifCountContainer}>
      <View style={Style().notifCountInnerContainer}>
        <Text align='center' color='white' style={{ fontSize: normalize(notif < 10 ? 10 : 8) }}>{notif}</Text>
      </View>
    </View>
  </TouchableOpacity>
);

const Style = (routeName?: string) => StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: screenWidth,
    height: Platform.OS === 'ios' ? normalize(80) : normalize(60),
    backgroundColor: routeName === 'Camera' ? colors.blackDefault : colors.white,
    shadowOpacity: 0.1,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 0,
    },
    shadowColor: routeName === 'Camera' || routeName === 'Order' ? 'transparent' : colors.darkBlue,
    paddingTop: Platform.OS === 'ios' ? normalize(30) : 0,
    paddingHorizontal: normalize(20),
  },

  notifBtnContainer: {
    height: '100%',
    width: 35,
    justifyContent: 'center',
    alignItems: 'center'
  },
  notifCountContainer: {
    position: 'absolute',
    width: normalize(20),
    height: normalize(20),
    right: -2,
    top: 6,
    borderWidth: 3,
    borderRadius: 100,
    borderColor: colors.white,
  },
  notifCountInnerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    borderRadius: 100,
    backgroundColor: colors.yobenBlue,
  },

  leftHeader: {
    flex: 2,
    alignItems: 'flex-start',
  },
  headerTitle: {
    flex: 8,
    alignItems: 'center',
  },
  rightHeader: {
    flex: 2,
    alignItems: 'flex-end',
  }
})

export default CustomHeader;