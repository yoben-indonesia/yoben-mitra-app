import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import { Text } from 'components';
import { colors } from 'consts';
import { MyOrder, OrderHistory } from 'screens';

const TabTop = createMaterialTopTabNavigator();

const OrderTopTabs: React.FC = () => {
  const options = ({ route }) => ({
    tabBarLabel: ({ focused }) => {
      return (
        <Text
          size='s'
          color={focused ? 'yobenBlue' : 'darkBlueTransparent'}
          text={route.name === 'MyOrder' ? 'Orderan Anda' : 'Riwayat Orderan'}
        />
      );
    },
  });

  return (
    <TabTop.Navigator
      initialRouteName='MyOrder'
      tabBarOptions={{ indicatorStyle: { backgroundColor: colors.yobenBlue } }}
      sceneContainerStyle={{ backgroundColor: 'white' }}
    >
      <TabTop.Screen name='MyOrder' component={MyOrder} options={options} />
      <TabTop.Screen name='OrderHistory' component={OrderHistory} options={options} />
    </TabTop.Navigator>
  );
}

export default OrderTopTabs;