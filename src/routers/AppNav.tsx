import 'react-native-gesture-handler';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Messaging from '@react-native-firebase/messaging';

import { saveFcmToken } from 'stores/action';
import { setNavigator } from 'helpers';
import config from './config';

interface AppNavProps {
  initialRouteName: string
}

const RootStack = createStackNavigator();

const AppNav: React.FC<AppNavProps> = ({ initialRouteName }) => {
  const dispatch = useDispatch();
  let navigator;

  React.useEffect(() => {
    const notifConfig = async () => {
      const authStatus = await Messaging().requestPermission();
      const enabled =
        authStatus === Messaging.AuthorizationStatus.AUTHORIZED ||
        Messaging.AuthorizationStatus.PROVISIONAL;
      if (enabled) {
        const fcmToken = await Messaging().getToken();
        dispatch(saveFcmToken(fcmToken))
      }
    };
    notifConfig();
  }, [])

  React.useEffect(() => {
    setNavigator(navigator)
  }, [navigator])

  const screenOptions: any = {
    headerTitleAlign: 'center',
    headerTitleStyle: {
      fontFamily: 'MavenPro-Bold',
      fontWeight: 'bold',
    }
  };

  return (
    <NavigationContainer ref={nav => { navigator = nav }}>
      <RootStack.Navigator initialRouteName={initialRouteName} headerMode='screen' screenOptions={{ ...screenOptions }}>
        {config.map((route, index) => <RootStack.Screen key={index} name={route.name} component={route.component} options={route.options} />)}
      </RootStack.Navigator>
    </NavigationContainer>
  );
}

export default AppNav;