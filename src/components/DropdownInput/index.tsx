import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import Text from '../Text';
import { normalize } from 'helpers';
import { themes, colors } from 'consts';

interface DropDownInputProps {
  onPress: () => void
  placeholder: string
  value: string
  customIcon?: any
  mr?: number,
  flex?: number,
}

const DropDownInput: React.FC<DropDownInputProps> = ({
  onPress,
  value,
  placeholder,
  customIcon,
  mr = 0,
  flex = 0,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={onPress}
      style={{
        ...Style(value).container,
        ...Style(flex).flex,
        ...Style(mr).mr,
      }}
    >
      <Text weight='regular' color={value ? 'black' : 'greyTextOri'} text={value || placeholder} />
      {
        customIcon ||
        <Icon
          name='chevron-down'
          size={normalize(18)}
          color={colors.yobenBlue}
        />
      }
    </TouchableOpacity>
  );
}

const margin = 1;
const flex = 1;
const Style = (params: any) => StyleSheet.create({
  container: {
    paddingHorizontal: normalize(20),
    paddingVertical: normalize(15),
    marginBottom: normalize(20),
    borderRadius: 10,
    borderWidth: 1,
    borderColor: params ? colors.yobenBlue : colors.inputBorder,
    ...themes.rowCenterBetween
  },
  mr: {
    marginRight: normalize(margin * params)
  },
  flex: {
    flex: params * flex,
  }
})

export default DropDownInput;