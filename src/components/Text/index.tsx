import React from 'react';
import { Text, StyleSheet } from 'react-native';

import { normalize } from 'helpers';
import { colors } from 'consts';

interface TextProps {
  size?: string
  color?: string
  text?: string
  weight?: string
  align?: string
  mb?: number
  mt?: number
  mr?: number
  ml?: number
  style?: any
  numOfLines?: number
}

const StaticText: React.FC<TextProps> = ({ children, size, color, weight = 'bold', mb = 0, mt = 0, mr = 0, ml = 0, text, align, style, numOfLines = 0 }) => {
  return (
    <Text
      numberOfLines={numOfLines}
      ellipsizeMode='tail'
      style={{
        ...Style()[weight],
        ...Style()[size],
        ...Style(mb).mb,
        ...Style(mt).mt,
        ...Style(mr).mr,
        ...Style(ml).ml,
        ...(align && Style(align).textAlign),
        textAlign: align,
        color: color ? colors[color] : color,
        ...style,
      }}
    >
      {children || text}
    </Text>
  )
}

const margin = 1;

const Style: any = (param: any) => StyleSheet.create({
  bold: {
    fontFamily: 'MavenPro-Bold',
  },
  regular: {
    fontFamily: 'MavenPro-Medium',
  },
  xs: {
    fontSize: normalize(12),
  },
  s: {
    fontSize: normalize(14),
  },
  m: {
    fontSize: normalize(16)
  },
  l: {
    fontSize: normalize(20)
  },
  xl: {
    fontSize: normalize(24)
  },
  mb: {
    marginBottom: margin * param
  },
  mt: {
    marginTop: margin * param
  },
  mr: {
    marginRight: margin * param
  },
  ml: {
    marginLeft: margin * param
  },
})

export default StaticText;