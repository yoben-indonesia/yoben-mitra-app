import React, { useState } from 'react';
import { TextInput, StyleSheet, View, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Text from '../Text';
import { colors } from 'consts';
import { normalize } from 'helpers';

interface InputProps {
  placeholder?: string,
  value?: string,
  name?: string,
  label?: string,
  type?: any,
  mb?: number,
  onChangeText?: any,
  multiline?: boolean,
  style?: any
}

const Input: React.FunctionComponent<InputProps> = ({
  placeholder,
  value,
  name,
  label = 'Label',
  type = 'none',
  mb = 0,
  onChangeText,
  multiline,
  style
}) => {

  const [showLabel, setShowLabel] = useState(false)
  const [showPassword, setShowPassword] = useState(false)
  const [isFocused, setIsFocused] = useState(false)
  const [labelColor, setLabelColor] = useState('darkBlueGrey')

  function handleOnChangeText(value: string) {
    return onChangeText(value, name)
  }

  function handleOnFocus() {
    setShowLabel(true);
    setIsFocused(true)
    setLabelColor('yobenBlue')
  }

  function handleOnBlur() {
    !value && setShowLabel(false)
    setIsFocused(false)
    setLabelColor('darkBlueGrey')
  }

  function onPressViewPassword() {
    if (showPassword) setShowPassword(false)
    else setShowPassword(true)
  }

  return (
    <View>
      {
        showLabel && !multiline &&
        <Text size='xs' weight='regular' mb={normalize(5)} color={labelColor} text={label}></Text>
      }
      <View
        style={{
          ...Style(isFocused).inputBox,
          ...Style(mb).mb
        }}
      >
        <TextInput
          placeholder={placeholder}
          value={value}
          onChangeText={handleOnChangeText}
          textContentType={type}
          autoCorrect={false}
          spellCheck={false}
          autoCapitalize='none'
          onFocus={handleOnFocus}
          onBlur={handleOnBlur}
          secureTextEntry={!showPassword && type === 'password' && true}
          style={{
            ...Style().textInput,
            ...style,
          }}
          multiline={multiline}
        />
        {
          type === 'password' &&
          <View style={Style().icon}>
            <Icon name='eye' color='#D0D0D0' size={normalize(14)} onPress={onPressViewPassword}></Icon>
          </View>
        }
      </View>
    </View>
  )
}

const mb = 1;

const Style = (param?: any) => StyleSheet.create({
  inputBox: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: param ? colors.yobenBlue : colors.inputBorder,
  },
  textInput: {
    fontSize: normalize(14),
    fontFamily: 'MavenPro-Medium',
    paddingHorizontal: normalize(15),
    paddingVertical: normalize(10),
    width: '100%',
    paddingRight: normalize(35),
  },
  icon: {
    position: 'absolute',
    right: normalize(15),
  },
  mb: {
    marginBottom: mb * param
  }
})

export default Input;