import React from 'react';
import { TouchableOpacity } from 'react-native';

import { colors } from 'consts';
import { normalize } from 'helpers';
import Text from '../Text';

interface PillProps {
  isSelected?: boolean
  onPress?: () => void
  text?: string
  mr?: number
  mb?: number
}

const Pill: React.FC<PillProps> = ({
  isSelected,
  onPress,
  text,
  mr = 0,
  mb = 0,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        ...Style.pill(isSelected),
        ...Style.mr(mr),
        ...Style.mb(mb)
      }}
      activeOpacity={1}
    >
      <Text
        text={text}
        weight='bold'
        color={isSelected ? 'white' : 'yobenBlue'}
      />
    </TouchableOpacity>
  );
}

const Style: any = {
  pill: (isSelected) => ({
    backgroundColor: isSelected ? colors.yobenBlue : colors.yobenBlueTransparent,
    alignSelf: 'flex-start',
    paddingHorizontal: normalize(15),
    paddingVertical: normalize(10),
    borderRadius: 50,
  }),
  mr: (mr) => ({
    marginRight: mr
  }),
  mb: (mb) => ({
    marginBottom: mb
  })
}

export default Pill;