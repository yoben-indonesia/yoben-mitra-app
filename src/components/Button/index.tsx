import React from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, View, Image } from 'react-native'

import Text from '../Text';
import { colors, images } from 'consts';

interface ButtonProps {
  text: string
  onPress: () => void
  type?: string
  model?: string
  shadow?: boolean
  disabled?: boolean
  mb?: number
  mt?: number
  mr?: number
  ml?: number
  style?: any
  loading?: boolean
  activeOpacity?: number
}

const Button: React.FC<ButtonProps> = ({ onPress, text, type, shadow, disabled, children, mb = 0, mt = 0, mr = 0, ml = 0, model, style, loading = false, activeOpacity = 0 }) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      disabled={disabled}
      activeOpacity={activeOpacity}
      style={{
        ...(type !== 'text' && Style(loading).theme),
        ...(model === 'outline' && Style().themeOutline),
        ...(model === 'outline-red' && Style().themeOutlineRed),
        ...(type === 'large' && Style().large),
        ...(shadow && Style().shadow),
        ...(disabled && Style().disabled),
        ...Style(mb).mb,
        ...Style(mt).mt,
        ...Style(mr).mr,
        ...Style(ml).ml,
        ...style
      }}
    >
      {
        loading ?
          <Image source={images.InfinityLoading} style={{ width: 30, height: 20 }} />
          : children ||
          <Text
            text={text}
            weight='bold'
            color={type === 'text' || model === 'outline' ? 'yobenBlue' : model === 'outline-red' ? 'red' : 'white'}
            size='s'
          />
      }
    </TouchableOpacity>
  )
}

const margin = 1;
const Style: any = (param: any) => StyleSheet.create({
  large: {
    width: '100%'
  },
  theme: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 8,
    backgroundColor: param ? 'rgba(110, 205, 221, 0.2)' : colors.yobenBlue,
    alignItems: 'center',
    alignSelf: 'flex-start',
  },
  themeOutline: {
    backgroundColor: 'transparent',
    borderWidth: 1.5,
    borderColor: colors.yobenBlue,
  },
  themeOutlineRed: {
    backgroundColor: 'transparent',
    borderWidth: 1.5,
    borderColor: colors.red,
  },
  shadow: {
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 0,
    },
    shadowColor: colors.buttonShadow,
  },
  disabled: {
    shadowOpacity: 0,
    backgroundColor: 'rgba(110, 205, 221, 0.2)'
  },
  mb: {
    marginBottom: margin * param
  },
  mt: {
    marginTop: margin * param
  },
  mr: {
    marginRight: margin * param
  },
  ml: {
    marginLeft: margin * param
  },
})

export default Button;