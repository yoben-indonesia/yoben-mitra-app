import React, { useState, useEffect } from "react";
import {
  Modal,
  StyleSheet,
  View,
  Animated
} from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Text from '../Text';
import { colors, screenHeight, screenWidth } from 'consts';
import { normalize } from 'helpers';
import Button from 'components/Button';

interface ModalProps {
  open?: boolean
  ilu?: React.ReactNode
  title?: string
  desc?: string
  loading?: boolean
  leftBtn?: {
    text?: string
    action?: () => void
  }
  rightBtn?: {
    text?: string
    action?: () => void
  }
  onClose?: () => void
}

const App: React.FC<ModalProps> = ({ open, onClose, ilu, title, desc, leftBtn, rightBtn, loading }) => {
  const slideUpValue = new Animated.Value(-screenHeight)
  const opacityValue = new Animated.Value(0)

  useEffect(() => {
    toggleModal()
  }, [open])

  function onCloseModal() {
    closeSelection()
  }

  function toggleModal() {
    Animated.parallel([
      Animated.spring(
        slideUpValue,
        {
          toValue: 0,
          velocity: 3,
          tension: 1,
          friction: 5,
          useNativeDriver: true
        }
      ),
      Animated.timing(
        opacityValue,
        {
          toValue: 1,
          duration: 500,
          useNativeDriver: true
        }
      )
    ]).start();
  }

  function closeSelection() {
    Animated.parallel([
      Animated.spring(
        slideUpValue,
        {
          toValue: screenHeight,
          velocity: 5,
          tension: 2,
          friction: 8,
          useNativeDriver: true
        }
      ),
      Animated.timing(
        opacityValue,
        {
          toValue: 0,
          duration: 500,
          useNativeDriver: true
        }
      )
    ]).start();

    setTimeout(() => {
      onClose()
    }, 300)
  }

  return (
    <Modal
      animationType="none"
      transparent={true}
      visible={open}
    >
      <View style={Style.modalContainer}>
        <Animated.View style={{ ...Style.modalContentContainer, transform: [{ translateY: slideUpValue }], opacity: opacityValue }}>
          <View style={Style.close}>
            <Icon
              name='close'
              size={normalize(26)}
              color='#D0D0D0'
              onPress={onCloseModal}
            />
          </View>
          <View style={Style.ilu}>
            {ilu}
          </View>
          <Text size='m' color='yobenBlue' align='center' mb={10}>{title}</Text>
          <Text size='s' color='darkBlueGrey' weight='regular' align='center' mb={20}>{desc}</Text>
          <View style={Style.btnContainer}>
            <View style={{ flex: 1, marginRight: normalize(20) }}>
              <Button text={leftBtn?.text || 'Batalkan'} model='outline' type='large' onPress={leftBtn?.action || onCloseModal}></Button>
            </View>
            <View style={{ flex: 1 }}>
              <Button text={rightBtn?.text || 'Ok'} type='large' loading={loading} onPress={rightBtn?.action}></Button>
            </View>
          </View>
        </Animated.View>
      </View>
    </Modal>
  );
};

const Style = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
    width: screenWidth,
    height: screenHeight,
  },
  modalContentContainer: {
    width: '90%',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    padding: normalize(20),
    position: 'absolute'
  },
  ilu: {
    marginBottom: normalize(20)
  },
  close: {
    width: '100%',
    alignItems: 'flex-end',
    marginBottom: normalize(30)
  },
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%'
  }
});

export default App;