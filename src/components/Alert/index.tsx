import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Animated } from 'react-native';

import { hideAlert } from 'stores/action';
import { normalize, responsive } from 'helpers';
import { colors } from 'consts';
import { alertData } from 'selectors';

import Text from '../Text';

const Alert: React.FC = () => {
  const dispatch = useDispatch();
  const alert = useSelector(alertData);
  let slideValue = new Animated.Value(-150);

  const handleAnimation = async (value: number) => {
    await Animated.spring(
      slideValue,
      {
        toValue: value,
        velocity: 3,
        tension: 2,
        friction: 8,
        useNativeDriver: true
      }
    ).start();
  }

  if (alert.isVisible) {
    setTimeout(() => {
      slideValue.setValue(0);
      handleAnimation(-150);
    }, 2000)

    setTimeout(async () => {
      dispatch(hideAlert());
    }, 2000)

    handleAnimation(0);
  }

  return alert.isVisible && (
    <Animated.View style={{ ...Style.alertContainer(alert.status), transform: [{ translateY: slideValue }] }}>
      <Text color='white' text={alert.message} />
    </Animated.View>
  );
}

const Style: any = {
  alertContainer: (status: string) => ({
    justifyContent: 'center',
    backgroundColor: status === 'bad' ? colors.red : colors.yobenBlue,
    paddingVertical: 15,
    paddingHorizontal: normalize(20),
    borderRadius: 10,
    position: 'absolute',
    top: 100,
    alignSelf: 'center',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 0,
    },
    shadowColor: 'rgba(45, 104, 140, 0.1)',
    elevation: 3
  })
}

export default Alert;