import React from 'react';
import { View, Image, Modal } from 'react-native';
import { useSelector } from 'react-redux';

import { colors, screenHeight, screenWidth, images } from 'consts';
import { isLoading } from 'selectors';

interface LoadingProps {
  backdrop?: boolean
}

const Loading: React.FC<LoadingProps> = ({ backdrop = false }) => {
  const loading = useSelector(isLoading);

  return loading && (
    <Modal transparent={true}>
      <View style={Style.loadingWrapper(backdrop)}>
        <View style={Style.loadingIconContainer}>
          <Image source={images.EllipsisLoading} style={{ width: 40, height: 40 }} />
        </View>
      </View>
    </Modal>
  );
}

const Style: any = {
  loadingWrapper: (backdrop: boolean) => ({
    width: screenWidth,
    height: screenHeight,
    // backgroundColor: backdrop ? 'rgba(0,0,0,0.1)' : 'none',
    backgroundColor: 'rgba(0,0,0,0.2)',
    justifyContent: 'center',
    alignItems: 'center',
  }),
  loadingIconContainer: {
    padding: 5,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5
  }
}

export default Loading;