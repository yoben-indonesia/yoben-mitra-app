import React, { useEffect } from "react";
import {
  Modal,
  StyleSheet,
  Animated,
  TouchableOpacity
} from "react-native";

import { screenHeight, screenWidth } from 'consts';
import { normalize } from 'helpers';

interface ModalProps {
  open?: boolean,
  close?: () => void,
  position?: 'string'
}

const ModalPopup: React.FC<ModalProps> = ({ open, close, children }) => {
  const slideUpValue = new Animated.Value(200);

  useEffect(() => {
    toggleDrawer()
  }, [open])

  function toggleDrawer() {
    Animated.spring(
      slideUpValue,
      {
        toValue: 0,
        velocity: 5,
        tension: 2,
        friction: 8,
        useNativeDriver: true
      }
    ).start();
  }

  function closeDrawer() {
    Animated.spring(
      slideUpValue,
      {
        toValue: screenHeight,
        velocity: 5,
        tension: 2,
        friction: 8,
        useNativeDriver: true
      }
    ).start();

    setTimeout(() => {
      close()
    }, 300)
  }

  return (
    <Modal
      animationType="none"
      transparent={true}
      visible={open}
    >
      <TouchableOpacity style={Style.drawerContainer} onPress={closeDrawer} activeOpacity={1}>
        <Animated.View style={{ ...Style.drawerContentContainer, transform: [{ translateY: slideUpValue }] }}>
          {children}
        </Animated.View>
      </TouchableOpacity>
    </Modal>
  );
};

const Style = StyleSheet.create({
  drawerContainer: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'flex-end',
    width: screenWidth,
    height: screenHeight,
  },
  drawerContentContainer: {
    position: 'absolute',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    padding: normalize(20),
    backgroundColor: 'white',
  },
});

export default ModalPopup;