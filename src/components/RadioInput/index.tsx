
import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';

import { themes, colors } from 'consts';
import { normalize } from 'helpers';
import { Text } from 'components';

interface RadioInputProps {
  onSelect: () => void
  value: string
  selectedValue: string
}

const RadioInput: React.FC<RadioInputProps> = ({ children, onSelect, value, selectedValue }) => {
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={onSelect}
    >
      <View style={Style().contentContainer}>
        {children}
        <View style={Style(value, selectedValue).radio} />
      </View>
    </TouchableOpacity>
  );
}

const Style = (value?: string, selectedValue?: string) => StyleSheet.create({
  contentContainer: {
    ...themes.rowCenterBetween,
    ...themes.borderBottom,
    paddingVertical: normalize(15)
  },
  radio: {
    width: normalize(20),
    height: normalize(20),
    borderWidth: value === selectedValue ? 6 : 1,
    borderColor: colors.yobenBlue,
    borderRadius: 100
  }
})
export default RadioInput;