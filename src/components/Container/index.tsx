import React from 'react';
import { StyleSheet, ScrollView, View, Platform } from 'react-native';

import { colors } from 'consts';
import { normalize } from 'helpers';

interface ContainerProps {
  style?: any,
  scroll?: boolean
}

const Container: React.FC<ContainerProps> = ({ children, style, scroll = false }) => {
  const scrollableScreen: any = () => (
    <ScrollView 
    bounces={false} 
    contentContainerStyle={{ ...Style.content, flex: 0, ...style }}
    showsVerticalScrollIndicator={false}
    >
      {children}
    </ScrollView>
  );

  const plainScreen: any = () => (
    <View style={{ ...Style.content, ...style }}>
      {children}
    </View>
  );

  return scroll ? scrollableScreen() : plainScreen();
}

const Style = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: colors.white,
    marginTop: Platform.OS === 'ios' ? normalize(20) : 0,
  },
})

export default Container;
