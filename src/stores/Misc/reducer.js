import {dispatch} from 'consts';

const initialState = {
  loading: {
    isLoading: false,
    message: '',
  },
  alert: {
    isVisible: false,
    message: '',
    status: 'bad',
  },
  lazyLoad: {
    ballance: false,
    orders: false,
    ordersDone: true,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case dispatch.START_LOADING:
    case dispatch.STOP_LOADING:
      return {...state, loading: action.data};

    case dispatch.START_LAZY_LOAD:
    case dispatch.STOP_LAZY_LOAD:
      return {
        ...state,
        lazyLoad: {
          ...state.lazyLoad,
          [action.data.fieldName]: action.data.isLoading,
        },
      };

    case dispatch.SHOW_ALERT:
    case dispatch.HIDE_ALERT:
      return {...state, alert: action.data};

    default:
      return state;
  }
};
