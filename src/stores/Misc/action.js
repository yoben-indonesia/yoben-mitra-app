import {dispatch} from 'consts';

const startLoading = (message = '') => ({
  type: dispatch.START_LOADING,
  data: {
    isLoading: true,
    message,
  },
});

const stopLoading = (message = '') => ({
  type: dispatch.STOP_LOADING,
  data: {
    isLoading: false,
    message,
  },
});

const startLazyLoad = (fieldName) => ({
  type: dispatch.START_LAZY_LOAD,
  data: {
    fieldName,
    isLoading: true,
  },
});

const stopLazyLoad = (fieldName) => ({
  type: dispatch.START_LAZY_LOAD,
  data: {
    fieldName,
    isLoading: false,
  },
});

const showAlert = (message = '', status = 'bad') => ({
  type: dispatch.SHOW_ALERT,
  data: {
    isVisible: true,
    message,
    status,
  },
});

const hideAlert = (message) => ({
  type: dispatch.HIDE_ALERT,
  data: {
    isVisible: false,
    message,
  },
});

export {
  startLoading,
  stopLoading,
  startLazyLoad,
  stopLazyLoad,
  showAlert,
  hideAlert,
};
