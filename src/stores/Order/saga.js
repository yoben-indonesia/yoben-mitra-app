import {takeLatest, put, call} from 'redux-saga/effects';
import {request} from 'helpers';
import {dispatch, endpoints} from 'consts';
import {
  startLoading,
  stopLoading,
  startLazyLoad,
  stopLazyLoad,
  showAlert,
  setOrders,
  setOrderDetail,
  setOrdersDone,
  setUpdateStatus,
  setServiceDuration,
  setOrdersAnalytics,
  setUnassignedOrders,
} from '../action';

function* getOrders() {
  try {
    yield put(startLoading());
    yield put(startLazyLoad('orders'));

    const {code, data, message} = yield call(request, endpoints.getOrders);

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(setOrders(data.results.reverse()));
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }

  yield put(stopLoading());
  yield put(stopLazyLoad('orders'));
}

function* getOrdersDone() {
  try {
    yield put(startLoading());
    yield put(startLazyLoad('ordersDone'));

    const {code, data, message} = yield call(request, endpoints.getOrdersDone);

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(setOrdersDone(data.results));
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }

  yield put(stopLoading());
  yield put(stopLazyLoad('ordersDone'));
}

function* getOrdersAnalytics() {
  try {
    const {code, data, message} = yield call(
      request,
      endpoints.getOrdersAnalytics,
    );
    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(
        setOrdersAnalytics({
          today: data.daily,
          thisWeek: data.weekly,
          thisMonth: data.monthly,
        }),
      );
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }
}

function* getOrderDetail({orderCode, cb}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(
      request,
      endpoints.getOrderDetail(orderCode),
    );

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(setOrderDetail(data));
      cb && cb();
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }
  yield put(stopLoading());
}

function* updateStatus({form, cb}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(
      request,
      endpoints.updateStatus,
      form,
    );

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(
        setUpdateStatus({
          code: form.order_code,
          status: form.status,
        }),
      );

      cb && cb();
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }

  yield put(stopLoading());
}

function* updateServiceDuration({form, cb}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(
      request,
      endpoints.updateServiceDuration,
      form,
    );

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(showAlert('Durasi berhasil ditambahkan', 'ok'));
      cb && cb();
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }
  yield put(stopLoading());
}

function* getServiceDuration({serviceCode}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(
      request,
      endpoints.getServiceDuration(serviceCode),
    );

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(setServiceDuration(data.results));
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }

  yield put(stopLoading());
}

function* getUnassignedOrders({cb}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(request, endpoints.getUnassignedOrders());

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(setUnassignedOrders(data || []));
      cb && cb();
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }

  yield put(stopLoading());
}

function* postUnassignedOrders({serviceCode, cb}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(request, endpoints.postUnassignedOrders(serviceCode));
    
    cb && cb({code, message});
  } catch (error) {
    yield put(showAlert(error.message));
  }

  yield put(stopLoading());
}

function* getUnassignedDetail({serviceCode}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(request, endpoints.getUnassignedDetail(serviceCode));

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(setOrderDetail(data));
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }

  yield put(stopLoading());
}

export default function* rootSaga() {
  yield takeLatest(dispatch.GET_ORDERS, getOrders);
  yield takeLatest(dispatch.GET_ORDERS_DONE, getOrdersDone);
  yield takeLatest(dispatch.GET_ORDER_DETAIL, getOrderDetail);
  yield takeLatest(dispatch.UPDATE_STATUS, updateStatus);
  yield takeLatest(dispatch.UPDATE_DURATION, updateServiceDuration);
  yield takeLatest(dispatch.GET_SERVICE_DURATION, getServiceDuration);
  yield takeLatest(dispatch.GET_ORDERS_ANALYTICS, getOrdersAnalytics);
  yield takeLatest(dispatch.GET_UNASSIGNED_ORDERS, getUnassignedOrders);
  yield takeLatest(dispatch.POST_UNASSIGNED_ORDERS, postUnassignedOrders);
  yield takeLatest(dispatch.GET_UNASSIGNED_DETAIL, getUnassignedDetail);
}
