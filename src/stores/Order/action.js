import {dispatch} from 'consts';

const getOrders = () => ({
  type: dispatch.GET_ORDERS,
});

const setOrders = (data) => ({
  type: dispatch.SET_ORDERS,
  data,
});

const getOrdersDone = () => ({
  type: dispatch.GET_ORDERS_DONE,
});

const setOrdersDone = (data) => ({
  type: dispatch.SET_ORDERS_DONE,
  data,
});

const getOrderDetail = (orderCode, cb) => ({
  type: dispatch.GET_ORDER_DETAIL,
  orderCode,
  cb
});

const setOrderDetail = (data) => ({
  type: dispatch.SET_ORDER_DETAIL,
  data,
});

const updateStatus = (form, cb) => ({
  type: dispatch.UPDATE_STATUS,
  form,
  cb,
});

const setUpdateStatus = (data) => ({
  type: dispatch.SET_UPDATE_STATUS,
  data,
});

const updateDuration = (form, cb) => ({
  type: dispatch.UPDATE_DURATION,
  form,
  cb,
});

const getServiceDuration = (serviceCode) => ({
  type: dispatch.GET_SERVICE_DURATION,
  serviceCode,
});

const setServiceDuration = (data) => ({
  type: dispatch.SET_SERVICE_DURATION,
  data,
});

const setOrdersAnalytics = (data) => ({
  type: dispatch.SET_ORDERS_ANALYTICS,
  data,
});

const getOrdersAnalytics = () => ({
  type: dispatch.GET_ORDERS_ANALYTICS,
})

const getUnassignedOrders = (cb) => ({
  type: dispatch.GET_UNASSIGNED_ORDERS,
  cb
})

const setUnassignedOrders = (data) => ({
  type: dispatch.SET_UNASSIGNED_ORDERS,
  data
})

const postUnassignedOrders = (serviceCode, cb) => ({
  type: dispatch.POST_UNASSIGNED_ORDERS,
  serviceCode,
  cb
})

const getUnassignedDetail = (serviceCode) => ({
  type: dispatch.GET_UNASSIGNED_DETAIL,
  serviceCode
})

export {
  getOrders,
  setOrders,
  getOrdersDone,
  setOrdersDone,
  getOrderDetail,
  setOrderDetail,
  updateStatus,
  setUpdateStatus,
  updateDuration,
  getServiceDuration,
  setServiceDuration,
  setOrdersAnalytics,
  getOrdersAnalytics,
  getUnassignedOrders,
  setUnassignedOrders,
  postUnassignedOrders,
  getUnassignedDetail,
};
