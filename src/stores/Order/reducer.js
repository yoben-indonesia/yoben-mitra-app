import {dispatch} from 'consts';

const initialState = {
  data: [],
  detail: {},
  done: [],
  durations: [],
  unassignOrders: [],
  unassignDetail: {},
  analytics: {
    today: 0,
    thisWeek: 0,
    thisMonth: 0,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case dispatch.SET_ORDERS:
      return {...state, data: action.data};

    case dispatch.SET_ORDERS_DONE:
      return {...state, done: action.data};

    case dispatch.SET_ORDERS_ANALYTICS:
      return {...state, analytics: action.data};

    case dispatch.SET_ORDER_DETAIL:
      return {...state, detail: action.data};

    case dispatch.SET_SERVICE_DURATION:
      return {...state, durations: action.data};

    case dispatch.SET_UNASSIGNED_ORDERS:
      return {...state, unassignOrders: action.data};

    case dispatch.SET_UNASSIGNED_DETAIL:
      return {...state, unassignDetail: action.data};

    case dispatch.SET_UPDATE_STATUS:
      return {
        ...state,
        data: state.data.map((datum) => {
          if (datum.code === action.data.code) {
            datum.status = action.data.status;
          }

          return datum;
        }),
      };

    case dispatch.LOGOUT:
      return initialState;

    default:
      return state;
  }
};
