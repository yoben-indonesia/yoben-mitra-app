import {dispatch} from 'consts';

const initialState = {
  payment: [],
  balanceSummary: {},
  history: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case dispatch.SET_ALL_PAYMENT:
      return {...state, payment: action.data};

    case dispatch.SET_BALLANCE_SUMMARY:
      return {...state, balanceSummary: action.data};

    case dispatch.SET_HISTORY_TRANSACTION:
      return {...state, history: action.data};

    case dispatch.LOGOUT:
      return initialState;

    default:
      return state;
  }
};
