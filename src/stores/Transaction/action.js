import {dispatch} from 'consts';

const getAllPayment = () => ({
  type: dispatch.GET_ALL_PAYMENT,
});

const setAllPayment = (data) => ({
  type: dispatch.SET_ALL_PAYMENT,
  data,
});

const getBalanceSummary = () => ({
  type: dispatch.GET_BALLANCE_SUMMARY,
});

const setBalanceSummary = (data) => ({
  type: dispatch.SET_BALLANCE_SUMMARY,
  data,
});

const getHistoryTrans = () => ({
  type: dispatch.GET_HISTORY_TRANSACTION,
});

const setHistoryTrans = (data) => ({
  type: dispatch.SET_HISTORY_TRANSACTION,
  data,
});

const postOrderPayment = (form, cb) => ({
  type: dispatch.POST_ORDER_PAYMENT,
  form,
  cb,
});

const postTopUp = (form) => ({
  type: dispatch.POST_TOP_UP,
  form,
});

export {
  getAllPayment,
  setAllPayment,
  getBalanceSummary,
  setBalanceSummary,
  getHistoryTrans,
  setHistoryTrans,
  postOrderPayment,
  postTopUp,
};
