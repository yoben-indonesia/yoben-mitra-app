import {takeLatest, put, call} from 'redux-saga/effects';
import {request, resetNav} from 'helpers';
import {dispatch, endpoints} from 'consts';
import Language from 'language';
import {
  startLoading,
  stopLoading,
  startLazyLoad,
  stopLazyLoad,
  showAlert,
  setAllPayment,
  setBalanceSummary,
  setHistoryTrans,
} from '../action';

function* getAllPayments() {
  try {
    yield put(startLoading());
    yield put(startLazyLoad('ballance'));

    const {code, data} = yield call(request, endpoints.getAllPayments);

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(setAllPayment(data.results));
    }
  } catch (error) {}

  yield put(stopLoading());
  yield put(stopLazyLoad('ballance'));
}

function* getBallanceSummary() {
  try {
    yield put(startLoading());
    const {code, data} = yield call(request, endpoints.getBallanceSummary);

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(setBalanceSummary(data));
    }
  } catch (error) {}
  yield put(stopLoading());
}

function* getHistoryTrans() {
  try {
    yield put(startLoading());
    const {code, data} = yield call(request, endpoints.getHistoryTrans);

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(setHistoryTrans(data.results));
    }
  } catch (error) {}
  yield put(stopLoading());
}

function* postOrderPayment({form, cb}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(
      request,
      endpoints.postOrderPayment,
      form,
    );

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      cb && cb();
    }
  } catch (error) {}
  yield put(stopLoading());
}

function* postTopUp({form}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(
      request,
      endpoints.postTopUp,
      form,
    );

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      resetNav('Success', {
        title: Language.Success.TopUp.title,
        desc: Language.Success.TopUp.desc,
      });
    }
  } catch (error) {}
  yield put(stopLoading());
}

export default function* rootSaga() {
  yield takeLatest(dispatch.GET_ALL_PAYMENT, getAllPayments);
  yield takeLatest(dispatch.GET_BALLANCE_SUMMARY, getBallanceSummary);
  yield takeLatest(dispatch.GET_HISTORY_TRANSACTION, getHistoryTrans);
  yield takeLatest(dispatch.POST_ORDER_PAYMENT, postOrderPayment);
  yield takeLatest(dispatch.POST_TOP_UP, postTopUp);
}
