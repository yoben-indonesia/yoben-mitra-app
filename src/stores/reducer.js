import {combineReducers} from 'redux';

import misc from './Misc/reducer';
import mitra from './Mitra/reducer';
import order from './Order/reducer';
import transaction from './Transaction/reducer';

const rootReducers = combineReducers({
  misc,
  mitra,
  order,
  transaction,
});

export default rootReducers;
