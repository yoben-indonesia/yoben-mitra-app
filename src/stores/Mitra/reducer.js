import {dispatch} from 'consts';

const initialState = {
  data: {
    code: '',
    name: '',
    phone: '',
    avatar: '',
    token: '',
    user_preference: '',
    status: '',
    is_accept_order: false,
  },
  fcm_token: '',
  inbox: {
    count: 0,
    results: [],
  },
  otp: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case dispatch.SET_PROFILE:
      return {...state, data: action.data};

    case dispatch.SET_INBOX:
      return {...state, inbox: action.data};

    case dispatch.SAVE_FCM_TOKEN:
      return {...state, fcm_token: action.data};

    case dispatch.SET_OTP:
      return {...state, otp: action.data};

    case dispatch.SET_UPDATED_PREFERENCE:
      return {...state, data: {...state.data, user_preference: action.data}};
    
    case dispatch.SET_ACCEPT_ORDER:
      return {...state, data: {...state.data, is_accept_order: action.data}};

    case dispatch.LOGOUT:
      let fcm_token = state.fcm_token;
      return {
        ...initialState,
        fcm_token,
      };

    default:
      return state;
  }
};
