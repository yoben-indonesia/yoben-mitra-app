import {takeLatest, put, call, select} from 'redux-saga/effects';
import {request, navigate, resetNav, goBack} from 'helpers';
import {dispatch, endpoints} from 'consts';
import {
  startLoading,
  stopLoading,
  showAlert,
  setProfile,
  setOtp,
  setUpdatedPreference,
  setAcceptOrder,
} from '../action';

function* loginMitra({form}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(request, endpoints.login, form);

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(
        setProfile({
          code: data.code,
          name: data.name,
          phone: data.phone,
          avatar: data.image,
          token: data.token,
          status: data.status,
          user_preference: data.user_preference,
          is_accept_order: data.is_accept_order
        }),
      );

      if (data.status === 'active') {
        resetNav('Dashboard');
      } else {
        navigate('Otp', {phone: data.phone, fromLogin: true});
      }
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }
  yield put(stopLoading());
}

function* getInbox() {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(request, endpoints.getInbox);

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
    }
  } catch (error) {}
  yield put(stopLoading());
}

function* getOtp({form, cb}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(request, endpoints.getOtp, form);

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(setOtp(data.code));
      cb && cb();
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }
  yield put(stopLoading());
}

function* updatePassword({form}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(
      request,
      endpoints.setPassword,
      form,
    );

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      navigate('CustPreferences');
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }
  yield put(stopLoading());
}

function* updatePreferences({form, fromSetting}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(
      request,
      endpoints.setPreference,
      form,
    );

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      if (fromSetting) {
        goBack();
        yield put(setUpdatedPreference(form.user_preference));
        yield put(showAlert('Preferensi berhasil diupdate', 'ok'));
      } else {
        resetNav('Dashboard');
      }
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }
  yield put(stopLoading());
}

function* changePwd({form}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(
      request,
      endpoints.changePwd,
      form,
    );

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      if(form.otp) {
        resetNav('Dashboard');
        yield put(showAlert('Password lama berhasil diganti', 'ok'));
      } else navigate('Otp', {data: form, fromChangePwd: true, otp: data.code});
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }
  yield put(stopLoading());
}

function* forgotPwd({form}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(
      request,
      endpoints.forgotPwd(form?.token || ''),
      form,
    );

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      if (form.token) {
        resetNav('Login');
        yield put(showAlert('Password berhasil direset', 'ok'));
      }
      else if (form.otp) navigate('CreatePassword', { code: form.otp, phone: form.phone, fromForgotPwd: true, token: data.token })
      else navigate('Otp', {phone: form.phone, fromForgotPwd: true, otp: data.code});
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }
  yield put(stopLoading());
}

function* updateFcmToken({fcmToken}) {
  try {
    const {code, message} = yield call(
      request,
      endpoints.updateFcmToken,
      fcmToken,
    );

    if (code >= 400) {
      yield put(showAlert(message));
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }
}

function* postFeedback({form}) {
  try {
    yield put(startLoading());
    const {code, message} = yield call(
      request,
      endpoints.postFeedback,
      form,
    );

    if (code >= 400) {
      yield put(showAlert(message));
    } {
      goBack();
      yield put(showAlert('Ulasan berhasil dikirim', 'ok'));
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }

  yield put(stopLoading());
}

function* updateAcceptOrder({form, cb}) {
  try {
    yield put(startLoading());
    const {code, data, message} = yield call(
      request,
      endpoints.updateAcceptOrder,
      form,
    );

    if (code >= 400) {
      yield put(showAlert(message));
    } else {
      yield put(setAcceptOrder(data.is_accept_order))

      if (data.is_accept_order) cb && cb();
    }
  } catch (error) {
    yield put(showAlert(error.message));
  }

  yield put(stopLoading());
}

export default function* rootSaga() {
  yield takeLatest(dispatch.LOGIN, loginMitra);
  yield takeLatest(dispatch.GET_INBOX, getInbox);
  yield takeLatest(dispatch.GET_OTP, getOtp);
  yield takeLatest(dispatch.UPDATE_PASSWORD, updatePassword);
  yield takeLatest(dispatch.UPDATE_PREFERENCE, updatePreferences);
  yield takeLatest(dispatch.CHANGE_PASSWORD, changePwd);
  yield takeLatest(dispatch.FORGOT_PASSWORD, forgotPwd);
  yield takeLatest(dispatch.UPDATE_FCM_TOKEN, updateFcmToken);
  yield takeLatest(dispatch.POST_FEEDBACK, postFeedback);
  yield takeLatest(dispatch.UPDATE_ACCEPT_ORDER, updateAcceptOrder);
}
