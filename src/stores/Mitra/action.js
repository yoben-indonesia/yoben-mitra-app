import {dispatch} from 'consts';

const login = (form) => ({
  type: dispatch.LOGIN,
  form,
});

const setProfile = (data) => ({
  type: dispatch.SET_PROFILE,
  data,
});

const getInbox = () => ({
  type: dispatch.GET_INBOX,
});

const setInbox = (data) => ({
  type: dispatch.SET_INBOX,
  data,
});

const updatePassword = (form) => ({
  type: dispatch.UPDATE_PASSWORD,
  form,
});

const saveFcmToken = (data) => ({
  type: dispatch.SAVE_FCM_TOKEN,
  data,
});

const updateFcmToken = (fcmToken) => ({
  type: dispatch.UPDATE_FCM_TOKEN,
  fcmToken,
});

const updatePreference = (form, fromSetting) => ({
  type: dispatch.UPDATE_PREFERENCE,
  form,
  fromSetting,
});

const setUpdatedPreference = (data) => ({
  type: dispatch.SET_UPDATED_PREFERENCE,
  data
})

const readInbox = (data) => ({
  type: dispatch.READ_INBOX,
  data,
});

const getOtp = (form, cb) => ({
  type: dispatch.GET_OTP,
  form,
  cb,
});

const setOtp = (data) => ({
  type: dispatch.SET_OTP,
  data,
});

const changePwd = (form) => ({
  type: dispatch.CHANGE_PASSWORD,
  form
})

const forgotPwd = (form) => ({
  type: dispatch.FORGOT_PASSWORD,
  form,
})

const postFeedback = (form) => ({
  type: dispatch.POST_FEEDBACK,
  form
})

const updateAcceptOrder = (form, cb) => ({
  type: dispatch.UPDATE_ACCEPT_ORDER,
  form,
  cb
})

const setAcceptOrder = (data) => ({
  type: dispatch.SET_ACCEPT_ORDER,
  data
})

const logout = () => ({
  type: dispatch.LOGOUT,
});

export {
  login,
  setProfile,
  getInbox,
  setInbox,
  readInbox,
  updatePassword,
  updatePreference,
  setUpdatedPreference,
  updateFcmToken,
  saveFcmToken,
  getOtp,
  setOtp,
  changePwd,
  forgotPwd,
  postFeedback,
  updateAcceptOrder,
  setAcceptOrder,
  logout,
};
