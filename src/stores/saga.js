import {all} from 'redux-saga/effects';

import mitraSagas from './Mitra/saga';
import orderSagas from './Order/saga';
import transSagas from './Transaction/saga';

export default function* rootSaga() {
  yield all([mitraSagas(), orderSagas(), transSagas()]);
}
