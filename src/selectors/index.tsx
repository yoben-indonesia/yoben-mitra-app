export const isLoading = state => state.misc.loading.isLoading;
export const loagingMsg = state => state.misc.loading.message;
export const alertData = state => state.misc.alert;
export const lazyLoadData = state => state.misc.lazyLoad;

export const mitraData = state => state.mitra.data;
export const mitraName = state => state.mitra.data.name;
export const mitraPhone = state => state.mitra.data.phone;
export const mitraAvatar = state => state.mitra.data.avatar;
export const mitraCode = state => state.mitra.data.code;
export const mitraToken = state => state.mitra.data.token;
export const userPreference = state => state.mitra.data.user_preference;
export const isAcceptOrder = state => state.mitra.data.is_accept_order;
// export const inboxCount = state => state.mitra;
// export const inboxList = state => state.mitra.inbox.results;
export const fcmToken = state => state.mitra.fcm_token;
export const otpCode = state => state.mitra.otp;

export const ordersData = state => state.order.data;
export const ordersDoneData = state => state.order.done;
export const orderDetailData = state => state.order.detail;
export const serviceDurationData = state => state.order.durations;
export const ordersAnalyticsData = state => state.order.analytics;
export const unassignOrdersData = state => state.order.unassignOrders;

export const ballanceData = state => state.transaction.balanceSummary;
export const paymentData = state => state.transaction.payment;
export const historyTransData = state => state.transaction.history;