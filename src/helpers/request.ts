import { store } from '../stores';
import { resetNav } from './navigation';

export default async (endpoint: any, body?: any, callback?: any) => {
  try {
    const token = store.getState()?.mitra?.data?.token;
    const params = endpoint.params ? `/${endpoint.params}` : '';
    const url = `${endpoint.url}/${endpoint.path}${params}`;

    const makeRequest = await fetch(url, {
      method: endpoint.method,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: token || '',
      },
      body: JSON.stringify(body),
      mode: 'cors',
    });

    const response = await makeRequest.json();
    
    let message: string;
    if (response.code === 400) {
      message = response.message || `${Object.keys(response.validation)[0]}, ${
        Object.values(response.validation)[0][0]
      }`;
    } else if (response.code === 401) {
      resetNav('Login');
      store.dispatch({
        type: 'LOGOUT'
      })
      return;
    } else {
      message = response.message;
    }

    const data = {
      code: response.code,
      data: response.data,
      message,
    };
    console.log(`${endpoint.method}/${endpoint.path.toUpperCase()}`, data);

    return data;
  } catch (error) {
    console.log('ERRORRRRRRRRRR', error);
    throw error;
  }
};
