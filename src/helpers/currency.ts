const currency = (value: number, separator = ',') => {
  let valueRounded = Math.round(value);
  let stringAmount = valueRounded.toString().split('').reverse().join('');
  let currency = '';
  let result = '';

  // if (env.language === 'bahasa') {
  if (true) {
    currency = 'Rp ';

    for (let i = 0; i < stringAmount.length; i++) {
      if (i % 3 == 0) {
        result += stringAmount.substr(i, 3) + separator;
      }
    }

    return (
      currency +
      result
        .split('', result.length - 1)
        .reverse()
        .join('')
    );
  }
};

export default currency;
