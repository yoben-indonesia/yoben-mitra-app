import { useNavigation, CommonActions } from '@react-navigation/native';

const config: any = {};

export function setNavigator(nav) {
  if (nav) {
    config.navigator = nav;
  }
}

export function navigate(routeName: string, params?: any) {
  if (config.navigator && routeName) {
    config.navigator.navigate(routeName, params);
  }
}

export function resetNav(routeName: string, params?: any) {
  if (config.navigator && routeName) {
    const resetAction = CommonActions.reset({
      index: 0,
      routes: [
        {
          name: routeName,
          params,
        },
      ],
    });
    config.navigator.dispatch(resetAction);
  }
}

export function goBack() {
  if (config.navigator) {
    config.navigator.goBack();
  }
}

export function goBackToTop() {
  if (config.navigator) {
    config.navigator.popToTop();
  }
}

export default function (routeName?: string, params?: any) {
  const navigation = useNavigation();
  const resetAction = CommonActions.reset({
    index: 0,
    routes: [
      {
        name: routeName,
        params,
      },
    ],
  });

  return {
    navigate: () => navigation.navigate(routeName, params),
    goBack: () => navigation.goBack(),
    reset: () => navigation.dispatch(resetAction),
  };
}
