export default {
  email(val: string = '') {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //eslint-disable-line
    return re.test(val);
  },
  phone(phone: string = '') {
    const re = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g; //eslint-disable-line
    return !!(phone.length > 7 && phone.length < 15 && re.test(phone));
  },
  name(val: string = '') {
    return !!(val.length > 4);
  },
  password(val: string = '') {
    return !!(val.length >= 5);
  },
  confirmPassword(val: string, pass: string) {
    return !!(val === pass);
  },
};
