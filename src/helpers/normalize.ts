import {Dimensions, Platform, PixelRatio} from 'react-native';

const {width: SCREEN_WIDTH} = Dimensions.get('window');

// based on figma canvas width
const scale = SCREEN_WIDTH / 360;

export const responsive = (size1 = 0, size2 = 0) => {
  if (SCREEN_WIDTH <= 320) {
    return size1;
  } else if (SCREEN_WIDTH <= 370) {
    return size2 * 0.8;
  } else {
    return size2;
  }
};

export const platformScale = (ios: number, android: number) =>
  normalize(Platform.OS === 'ios' ? ios : android);

export const platformOs = (ios: any, android: any) =>
  Platform.OS === 'ios' ? ios : android;

export default function normalize(size: number) {
  const newSize = size * scale;
  if (SCREEN_WIDTH > 320) {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  }
}
