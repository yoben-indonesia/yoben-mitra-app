import env from '../../env';

const accUrl = env.apiAccountUrl;
const transurl = env.apiTransUrl;

export default {
  login: {url: accUrl, method: 'POST', path: 'login'},
  getInbox: {url: accUrl, method: 'GET', path: 'inbox'},
  readInbox: (params: string) => ({
    url: accUrl,
    method: 'PUT',
    path: `inbox/${params}`,
  }),
  setPassword: {url: accUrl, method: 'PUT', path: 'set-password'},
  setFcmToken: {url: accUrl, method: 'PUT', path: 'fcm-token'},
  setPreference: {url: accUrl, method: 'PUT', path: 'user-preference'},
  getOtp: {url: accUrl, method: 'POST', path: 'otp'},
  changePwd: {url: accUrl, method: 'PUT', path: 'change-password'},
  forgotPwd: (params:string = '') => ({url: accUrl, method: 'POST', path: `forgot-password?token=${params}`}),
  updateFcmToken: { url: accUrl, method: 'PUT', path: 'fcm-token' },
  updateAcceptOrder: { url: accUrl, method: 'PUT', path: 'accept-order'},

  postFeedback: {url: transurl, method: 'POST', path: 'feedbacks'},

  getOrders: {url: transurl, method: 'GET', path: 'orders?status=mitra_assigned%26mitra_on_the_way%26start_service%26finish_service%26pending_payment'},
  getOrderDetail: (params: string) => ({
    url: transurl,
    method: 'GET',
    path: `orders?code=${params}`,
  }),
  getOrdersDone: {
    url: transurl,
    method: 'GET',
    path: 'orders?status=done',
  },
  updateStatus: {url: transurl, method: 'PUT', path: 'orders/update-status'},
  postUnassignedOrders:  (params = '') => ({url: transurl, method: 'POST', path: `orders/unassigned/${params}`}),
  getUnassignedOrders:  (limit = 99, skip = 0) => ({url: transurl, method: 'GET', path: `orders/unassigned?limit=${limit}&skip=${skip}`}),
  getUnassignedDetail: (params = '') => ({url: transurl, method: 'GET', path: `orders/unassigned?code=${params}`}),

  getServiceDuration: (params: string) => ({
    url: transurl,
    method: 'GET',
    path: `services/${params}/duration`,
  }),
  updateServiceDuration: {
    url: transurl,
    method: 'PUT',
    path: 'orders/update-duration',
  },
  getOrdersAnalytics: {
    url: transurl,
    method: 'GET',
    path: 'orders/analytic',
  },

  getAllPayments: {url: transurl, method: 'GET', path: 'payments'},
  getBallanceSummary: {
    url: transurl,
    method: 'GET',
    path: 'transactions/balance',
  },
  getHistoryTrans: {
    url: transurl,
    method: 'GET',
    path: 'transactions',
  },
  postOrderPayment: {
    url: transurl,
    method: 'POST',
    path: 'transactions/order',
  },
  postTopUp: {
    url: transurl,
    method: 'POST',
    path: 'transactions/topup',
  },
};
