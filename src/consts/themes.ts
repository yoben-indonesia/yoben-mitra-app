import colors from './colors';
import {StyleSheet, StyleProp} from 'react-native';
import {normalize} from 'helpers';
import {screenWidth} from './sizes';

interface ThemesType {
  rowCenter: any;
  rowCenterBetween: any;
  columnCenter: any;
  borderBottom: any;
  shadow: any;
  rnShadowConfig: (height: number) => void;
}

const rowCenter = {
  flexDirection: 'row',
  alignItems: 'center',
};

const themes: ThemesType = {
  rowCenter,
  rowCenterBetween: {
    ...rowCenter,
    justifyContent: 'space-between',
  },
  columnCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  shadow: {
    shadowOpacity: 0.1,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 0,
    },
  },

  borderBottom: {
    borderBottomWidth: 1,
    borderColor: colors.inputBorder,
  },

  rnShadowConfig: (height: number) => ({
    width: screenWidth,
    height: normalize(height),
    color: colors.darkBlue,
    border: 10,
    radius: 10,
    opacity: 0.04,
    x: 0,
    y: 3,
  }),
};

export default themes;
