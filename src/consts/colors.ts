const listOfColors = {
  yobenBlue: '#55BBCC',
  yobenBlueTransparent: 'rgba(85, 187, 204, 0.1)',
  darkBlue: '#2D688C',
  darkBlueTransparent: 'rgba(45, 104, 140, 0.3)',
  darkBlueGrey: '#646C71',

  black: '#313537',
  blackDefault: '#000',
  white: '#FFFFFF',
  red: '#DC7272',
  pink: 'pink',
  green: '#70D34E',

  background: '#FBFDFF',
  inputBorder: '#F2F2F2',
  greyText: '#D0D0D0',
  buttonShadow: 'rgba(110, 205, 221, 0.4)',
};

const Colors: typeof listOfColors = listOfColors;

export default Colors;
