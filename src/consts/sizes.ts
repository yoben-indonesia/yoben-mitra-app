import {Dimensions} from 'react-native';

const {height: screenHeight} = Dimensions.get('window');
const {width: screenWidth} = Dimensions.get('window');

export {screenHeight, screenWidth};
