export default {
  InfinityLoading: require('../assets/images/gif/infinityLoading.gif'),
  EllipsisLoading: require('../assets/images/gif/ellipsisLoading.gif'),
};
