import Man from './man.svg';
import Woman from './woman.svg';
import LogoYoben from './logoYoben.svg';

import IcBroom from './IcBroom.svg';
import IcTopUp from './topUp.svg';
import IcCamera from './IcCamera.svg';
import IcFlash from './IcFlash.svg';
import IcHome from './IcHome.svg';
import IcOrder from './IcOrder.svg';
import IcProfile from './IcProfile.svg';
import IcNotif from './IcNotif.svg';
import IcWallet from './IcWallet';

import IluCleaner from './IluCleaner.svg';
import IluImportant from './IluImportant.svg';
import IluNoOrder from './IluNoOrder.svg';
import IluOk from './IluOk.svg';
import IluOkSmall from './IluOkSmall.svg';
import IluSmile from './IluSmile.svg';
import IluRefresh from './IluRefresh.svg';
import IluCancel from './IluCancel.svg';

export {
  Man,
  Woman,
  LogoYoben,
  //ICons
  IcBroom,
  IcTopUp,
  IcCamera,
  IcFlash,
  IcHome,
  IcOrder,
  IcProfile,
  IcNotif,
  IcWallet,
  //Ilustrations
  IluCleaner,
  IluImportant,
  IluNoOrder,
  IluOk,
  IluOkSmall,
  IluSmile,
  IluRefresh,
  IluCancel,
};
