import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, Keyboard, KeyboardAvoidingView, Platform } from "react-native";
import { useDispatch, useSelector } from 'react-redux';

import { changePwd } from 'stores/action';
import { Container, Button, Text, Input, Loading } from 'components';
import Language from 'language';
import { normalize } from 'helpers';
import { isLoading } from 'selectors';

const Login: React.FC = () => {
  const dispatch = useDispatch();
  const loading = useSelector(isLoading);
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  const [input, setInput] = useState({
    password: '',
    new_password: '',
    confNewPass: '',
  })

  const handleOnChangeText = (value: any, name: any) => {
    return setInput({
      ...input,
      [name]: value
    });
  }

  const handleDisabled = () => {
    return (input.password && input.new_password && input.confNewPass && input.new_password === input.confNewPass);
  }

  const onPressChangePwd = () => {
    delete input.confNewPass;
    dispatch(changePwd(input));
  }

  return isMounted && (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
      <Container style={Style.container}>
        <Input
          placeholder={Language.Auth.login.placeholder2}
          name='password'
          label='Password Lama'
          value={input.password}
          type='password'
          mb={20}
          onChangeText={handleOnChangeText}
        />
        <Input
          placeholder='Password Baru'
          name='new_password'
          label='Password Baru'
          value={input.new_password}
          type='password'
          mb={20}
          onChangeText={handleOnChangeText}
        />
        <Input
          placeholder='Konfirmasi Password Baru'
          name='confNewPass'
          label='Konfirmasi Password Baru'
          value={input.confNewPass}
          type='password'
          mb={20}
          onChangeText={handleOnChangeText}
        />

        <TouchableOpacity activeOpacity={1} style={{ flex: 1 }} onPress={() => Keyboard.dismiss()} />
        <Button
          type='large'
          shadow={true}
          text={Language.Auth.login.button2}
          disabled={!handleDisabled()}
          onPress={onPressChangePwd}
          activeOpacity={1}
          loading={loading}
        />
      </Container>
    </KeyboardAvoidingView>
  )
}

const Style = StyleSheet.create({
  container: {
    paddingHorizontal: normalize(20),
    paddingTop: normalize(30),
    paddingBottom: normalize(40),
  }
})

export default Login;