import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import { getOrdersDone, getOrdersAnalytics } from 'stores/action';
import { Button, Container, Text } from 'components';
import { normalize } from 'helpers';
import { colors, themes } from 'consts';
import Language from 'language';
import { mitraData, ordersAnalyticsData } from 'selectors';
import { SaldoCard } from '../../ScreenComponent';

interface MyProfileProps {
  navigation: any
}

const MyProfile: React.FC<MyProfileProps> = ({ navigation }) => {
  const dispatch = useDispatch();
  const mitra = useSelector(mitraData);
  const analytics = useSelector(ordersAnalyticsData);
  const [isMounted, setIsMounted] = React.useState(false);

  React.useEffect(() => {
    setIsMounted(true);
    dispatch(getOrdersDone());
    dispatch(getOrdersAnalytics());
  }, [isMounted]);

  function onPressNavigate(routeName: string) {
    return () => navigation.navigate(routeName)
  }

  return isMounted && (
    <Container
      style={{ backgroundColor: colors.background }}
    >
      <View style={Style.introContainer}>
        <View style={Style.profile}>
          <View style={Style.avatarContainer}>
            <Image
              source={{ uri: mitra.avatar }}
              style={Style.avatarImage}
            />
          </View>
          <View>
            <Text
              color='black'
              mb={5}
              size='m'
              text={mitra.name}
            />
            <Text
              color='darkBlueGrey'
              weight='regular'
              size='s'
              mb={10}
              text={mitra.phone}
            />
            <Button
              text='Atur Profil'
              onPress={onPressNavigate('Setting')}
            />
          </View>
        </View>

        <View style={themes.rowCenter}>
          <View style={Style.orderTotalContainer}>
            <Text
              size='xs'
              weight='regular'
              align='center'
              color='darkBlueGrey'
            >
              <Text color='black'>{analytics.thisMonth}</Text> {Language.MyProfile.orderTotal}
            </Text>
          </View>
          <View style={Style.favouritesContainer}>
            <Text
              size='xs'
              weight='regular'
              align='center'
              color='darkBlueGrey'
            >
              <Text color='black'>0</Text> {Language.MyProfile.favTotal}
            </Text>
          </View>
        </View>
      </View>

      <View style={{ padding: normalize(20) }}>
        <SaldoCard />
        {/* <ServiceScoreCard /> */}
      </View>
    </Container>
  );
}

const Style = StyleSheet.create({
  introContainer: {
    backgroundColor: colors.white,
    padding: normalize(20),
    ...themes.borderBottom,
  },
  profile: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: normalize(20)
  },
  avatarContainer: {
    width: normalize(80),
    height: normalize(80),
    backgroundColor: colors.yobenBlue,
    borderRadius: 100,
    borderWidth: 4,
    borderColor: colors.white,
    marginRight: normalize(10)
  },
  avatarImage: {
    width: '100%',
    height: '100%',
    borderRadius: 100
  },
  orderTotalContainer: {
    flex: 1,
    borderRightWidth: 1,
    borderColor: colors.inputBorder,
    paddingRight: normalize(20),
    paddingLeft: normalize(10)
  },
  favouritesContainer: {
    flex: 1,
    paddingLeft: normalize(20),
    paddingRight: normalize(10)
  }
})

export default MyProfile;