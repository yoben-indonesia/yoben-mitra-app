import React from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useSelector, useDispatch } from 'react-redux';

import { logout } from 'stores/action';
import { Text, Container } from 'components';
import { normalize, navigation, resetNav } from 'helpers';
import { colors, themes } from 'consts';
import Language from 'language';

import env from '../../../../env';

interface SettingProps {
  // navigation: any
}

const Setting: React.FC<SettingProps> = () => {
  const dispatch = useDispatch();
  const [isMounted, setIsMounted] = React.useState(false);
  const listOfMenus = [
    {
      name: Language.Setting.tnc,
      action: navigation('TnC').navigate
    },
    {
      name: Language.Setting.changePwd,
      action: navigation('ChangePwd').navigate
    },
    {
      name: Language.Setting.custPreferences,
      action: navigation('CustPreferences', { fromSetting: true }).navigate
    },
    {
      name: Language.Setting.feedback,
      action: navigation('Feedback').navigate
    },
    {
      name: Language.Setting.logout,
      action: () => {
        resetNav('Login');
        dispatch(logout());
      },
    },
  ];

  React.useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  const renderMenu = () => (
    listOfMenus.map((menu, index) => (
      <TouchableWithoutFeedback key={index} onPress={menu.action}>
        <View style={{ ...themes.rowCenterBetween, ...themes.borderBottom, padding: normalize(20) }}>
          <Text
            size='s'
            weight='regular'
            color={index === 4 ? 'red' : 'black'}
            text={menu.name}
          />
          <Icon
            name='chevron-right'
            size={normalize(18)}
            color={index === 4 ? colors.red : colors.greyText}
          />
        </View>
      </TouchableWithoutFeedback>
    ))
  )

  return isMounted && (
    <Container>
      {renderMenu()}
      <View style={{ position: 'absolute', bottom: normalize(40), width: '100%' }}>
          <Text
            size='xs'
            weight='regular'
            color='black'
            align='center'
            text='App. Version'
          />
          <Text
            size='xs'
            weight='regular'
            color='black'
            align='center'
            text={env.appVersion}
          />
        </View>
    </Container>
  );
}

export default Setting;