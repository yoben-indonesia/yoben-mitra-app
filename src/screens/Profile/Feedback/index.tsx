import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { useDispatch } from 'react-redux';

import {
  Text,
  Container,
  Button,
  RadioInput,
  Input,
  DropdownInput,
  Drawer,
  Loading
} from 'components';
import { normalize, responsive } from 'helpers';
import { postFeedback } from 'stores/action';
import { screenHeight } from 'consts';

interface HelpProps {
  navigation: any
}

const Help: React.FC<HelpProps> = ({ navigation }) => {
  const dispatch = useDispatch();
  const [isMounted, setIsMounted] = useState(false);
  const [modalBottomVisible, setModalBottomVisible] = useState(false);
  const [selectedTopic, setSelectedTopic] = useState('');
  const [details, setDetails] = useState('');

  useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  function handleDisabled() {
    return (selectedTopic && details)
  }

  function setModalBottom(status: boolean) {
    return function () {
      setModalBottomVisible(status)
    }
  }

  function sendFeedback() {
    dispatch(postFeedback({
      category: selectedTopic,
      description: details,
    }))
  }

  function selectTopic(topic: string) {
    return function () {
      setModalBottom(false)();
      setSelectedTopic(topic);
    }
  }

  function handleOnChangeText(value: any) {
    setDetails(value)
  }

  const renderTopicList = () => {
    const topics = ['Resik', 'Bugar', 'Service', 'Features', 'Payment', 'Others'];

    return topics.map((topic, index) => (
      <RadioInput
        onSelect={selectTopic(topic)}
        selectedValue={selectedTopic}
        value={topic}
        key={index}
      >
        <Text color='black' weight='regular' text={topic} />
      </RadioInput>
    ));
  }

  return isMounted && (
    <Container style={Style.container}>
      <Text
        size='m'
        color='yobenBlue'
        mb={normalize(15)}
        text='Pilih Kategori'
      />
      <DropdownInput
        placeholder={'Pilih Kategori'}
        onPress={setModalBottom(true)}
        value={selectedTopic}
      />

      <Text
        size='m'
        color='yobenBlue'
        mb={normalize(15)}
        text='Deskripsi'
      />
      <Input
        name='details'
        value={details}
        placeholder='tulis apapun...'
        onChangeText={handleOnChangeText}
        mb={37}
        multiline
        style={Style.textArea}
      />

      <View style={{ flex: 1 }} />
      <Button
        shadow
        type='large'
        text='Kirim Ulasan'
        disabled={!handleDisabled()}
        onPress={sendFeedback}
      />

      <Loading />

      <Drawer open={modalBottomVisible} close={setModalBottom(false)}>
        <View style={{ width: '100%'}}>
          {renderTopicList()}
        </View>
      </Drawer>
    </Container>
  );
}

const Style = StyleSheet.create({
  container: {
    paddingHorizontal: normalize(20),
    paddingVertical: normalize(20),
  },
  successContent: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: responsive(20),
    height: screenHeight - responsive(160),
  },
  textArea: {
    height: normalize(250),
    textAlignVertical: 'top',
    paddingHorizontal: 20,
    paddingTop: normalize(20),
  }
});

export default Help;