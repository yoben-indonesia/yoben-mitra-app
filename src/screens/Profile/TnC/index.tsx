import React from 'react';

import { Text, Container } from 'components';
import { normalize } from 'helpers';
import Language from 'language';
const { terms, cond } = Language.TnC

const TnC = () => {
  const [isMounted, setIsMounted] = React.useState(false);

  React.useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  const renderText = (size: string = 's', text: string, mb: number = 10) => (
    <Text
      size={size}
      color='black'
      mb={normalize(mb)}
      weight={size === 's' ? 'regular' : 'bold'}
      text={text}
    />
  );

  return isMounted && (
    <Container scroll style={{ padding: normalize(20) }}>
      {renderText('m', terms)}
      {renderText('s', Language.LoremIpsum)}
      {renderText('s', Language.LoremIpsum)}
      {renderText('s', Language.LoremIpsum, 20)}

      {renderText('m', cond)}
      {renderText('s', Language.LoremIpsum)}
      {renderText('s', Language.LoremIpsum)}
      {renderText('s', Language.LoremIpsum)}
    </Container>
  );
}

export default TnC;