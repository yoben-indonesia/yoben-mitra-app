import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, StatusBar, Image } from 'react-native'
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/Ionicons'

import { screenWidth, colors } from 'consts';
import { normalize } from 'helpers';
import { Text } from 'components';
import { IcFlash } from 'svg';

interface CameraProps {
  navigation: any,
  route: any
}

const Camera: React.FC<CameraProps> = ({ route: { params }, navigation }) => {
  const [isMounted, setIsMounted] = useState(false);
  const [torch, setTorch] = useState('off');
  const [img, setImg] = useState('');

  useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  function toggleTorch() {
    torch === 'off' ? setTorch('torch') : setTorch('off')
  }

  async function takePicture(camera) {
    const options = { quality: 0.5, base64: true, forceUpOrientation: false, orientation: 'portrait' };
    const data = await camera.takePictureAsync(options);
    data?.base64 && setImg(`data:image/png;base64,${data.base64}`)
  };

  function onPressOk() {
    params.previewImage(img)
    navigation.pop()
  }

  const renderImgPreview = () => (
    <View style={Style.container}>
      <Image
        source={{ uri: img }}
        style={Style.imgPreview}
      />
      <View style={Style.successBtnContainer}>
        <Icon
          name='reload'
          size={normalize(25)}
          color={colors.white}
          style={Style.icReload}
          onPress={() => setImg('')}
        />

        <TouchableOpacity
          onPress={onPressOk}
          style={Style.okBtn}
        >
          <Icon
            name='paper-plane'
            size={normalize(30)}
            color={colors.white}
            style={{ transform: [{ rotateZ: '45deg', }] }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );

  const renderCameraContent = () => (
    <View style={Style.container}>
      <RNCamera
        style={Style.preview}
        type={RNCamera.Constants.Type.back}
        flashMode={RNCamera.Constants.FlashMode[torch]}
        autoFocus='on'
        captureAudio={false}
        pictureSize={'1280x720'}
      >
        {({ camera, status }) => {
          if (status !== 'READY') return null;
          return (
            <View style={Style.readyBtnContainer}>
              <IcFlash
                width={normalize(25)}
                height={normalize(25)}
                onPress={toggleTorch}
                style={Style.icFlash}
              />
              <TouchableOpacity
                onPress={() => takePicture(camera)}
                style={Style.capture}
              >
                <View style={Style.captureBtn} />
              </TouchableOpacity>
            </View>
          );
        }}
      </RNCamera>
      {renderFrame()}
    </View>
  );

  const renderFrame = () => (
    <View style={Style.frameContainer}>
      <View style={Style.frameStyle}>
        <View style={Style.notesContainer}>
          <Text
            size='xs'
            color='white'
            align='center'
            weight='regular'
            text='Pastikan struk ada di dalam bingkai yang telah tersedia'
          />
        </View>
      </View>
    </View>
  );

  return isMounted && (
    <>
      <StatusBar barStyle='light-content' />
      <View style={{ flex: 1 }}>
        <View style={Style.blackBar} />
        {img ? renderImgPreview() : renderCameraContent()}
      </View>
    </>
  )
}

const Style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    position: 'relative',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'black'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: colors.blackDefault
  },
  capture: {
    flex: 0,
    backgroundColor: colors.white,
    borderRadius: 500,
    alignSelf: 'center',
  },

  imgPreview: {
    width: '100%',
    height: '100%',
  },

  readyBtnContainer: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: '13%',
    width: screenWidth,
    backgroundColor: colors.blackDefault
  },

  successBtnContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    width: screenWidth,
    height: 100,
    bottom: 0,
    backgroundColor: colors.blackDefault,
  },

  icReload: {
    position: 'absolute',
    left: screenWidth / 5
  },

  icFlash: {
    position: 'absolute',
    left: screenWidth / 5
  },

  okBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    height: normalize(55),
    width: normalize(55),
    backgroundColor: colors.yobenBlue,
    borderRadius: 100,
  },

  captureBtn: {
    height: normalize(55),
    width: normalize(55),
    borderRadius: 100,
    backgroundColor: colors.white
  },

  frameContainer: {
    flex: 1,
    position: 'absolute',
    height: '87%',
    paddingVertical: normalize(20)
  },
  frameStyle: {
    flex: 1,
    borderWidth: 2,
    borderColor: 'white',
    width: screenWidth - 40,
    borderRadius: 10,
    borderStyle: 'dashed'
  },
  notesContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    width: screenWidth / 2,
    bottom: normalize(-10),
    paddingVertical: normalize(10),
    backgroundColor: 'rgba(0,0,0,0.8)',
    borderRadius: 50,
  },

  blackBar: {
    height: normalize(20),
    width: screenWidth,
    backgroundColor: colors.blackDefault
  }
});

export default Camera;