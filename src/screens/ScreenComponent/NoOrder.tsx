import React from 'react';
import { View } from 'react-native';

import { Text } from 'components';
import { normalize } from 'helpers';
import { IluNoOrder } from 'svg';

interface NoOrderProps {
  text: string
}

const NoOrder: React.FC<NoOrderProps> = ({ text }) => {
  return (
    <View style={Style.noOrder}>
      <IluNoOrder />
      <View style={Style.textContainer}>
        <Text
          size='m'
          color='yobenBlue'
          align='center'
          text={text}
        />
      </View>
    </View>
  );
}

const Style: any = {
  noOrder: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: normalize(-20),
  },
  textContainer: {
    paddingHorizontal: 30,
    marginTop: normalize(15)
  }
}
export default NoOrder;