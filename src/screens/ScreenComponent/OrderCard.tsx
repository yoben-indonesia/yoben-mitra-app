import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { BoxShadow } from 'react-native-shadow';
import moment from 'moment';

import { updateStatus } from 'stores/action';
import { navigate, normalize } from 'helpers';
import { Text, Button, Modal, Loading } from 'components';
import { IcBroom, IluImportant, IluCleaner } from 'svg';
import { colors, screenWidth } from 'consts';
import { mitraCode, isLoading } from 'selectors';

interface OrderCardProps {
  navigation?: any,
  item?: any,
  unassignOrder?: boolean
}

const OrderCard: React.FC<OrderCardProps> = ({ navigation, item, unassignOrder = false }) => {
  const dispatch = useDispatch();
  const code = useSelector(mitraCode);
  const loading = useSelector(isLoading);
  const [modalVisible, setModalVisible] = useState(false);
  const [pressedCode, setPressedCode] = useState('');
  const [modalContent, setModalContent] = useState({
    ilu: <IluImportant />,
    title: '',
    desc: '',
    type: '',
    itemId: ''
  });

  const shadowOpt = {
    width: Math.floor(screenWidth - normalize(40)),
    height: normalize(155),
    color: colors.darkBlue,
    border: 10,
    radius: 10,
    opacity: 0.05,
    x: 0,
    y: 5,
    style: { marginBottom: normalize(20) }
  }

  function closeModal() {
    setModalVisible(false);
  }

  function modalBtnRightAction() {
    if (modalContent.type === 'mulai') {
      handleUpdateStatus('start_service')();
    } else {
      handleUpdateStatus('finish_service')();
    }
  }

  function onPressStart() {
    setModalVisible(true);
    setModalContent({
      ilu: <IluImportant width={100} height={100} />,
      title: 'Apakah Anda sudah tiba di lokasi orderan?',
      desc: 'Pastikan Anda sudah tiba di lokasi orderan, baru klik button Mulai.',
      type: 'mulai',
      itemId: item.id
    })
  }

  function onPressFinish() {
    setModalVisible(true);
    setModalContent({
      ilu: <IluCleaner width={100} height={100} />,
      title: 'Apakah Anda sudah selesai melakukan pelayanan?',
      desc: 'Pastikan Anda sudah selesai melakukan pelayanan Anda.',
      type: 'selesai',
      itemId: item.id
    })
  }

  function handleUpdateStatus(status: string, code?: any) {
    return () => {
      setPressedCode(item.code);
      dispatch(
        updateStatus(
          {
            order_code: item.code,
            status: status,
            mitra_code: code
          },
          () => {
            status === 'start_service' && navigation.navigate('OrderDetails', { item, modalContent, onPressFinish, onPressStart });
            status === 'finish_service' && navigate('Payment', { code: item.code });
            setModalVisible(false);
          }
        )
      );
    };
  }

  const renderButton = (code) => {
    if (unassignOrder) {
      return (
        <Button
          text='Lihat'
          type='large'
          onPress={() => navigation.navigate('OrderDetails', { item, modalContent, onPressFinish, onPressStart, image: item.service.image, unassignOrder })}
        />
      )
    } else if (item.status === 'mitra_assigned') {
      return (
        <Button
          text='Berangkat'
          type='large'
          loading={loading && pressedCode === code}
          // disabled={momentParse(item.work_time) !== momentParse(Date.now())}
          onPress={handleUpdateStatus('mitra_on_the_way', code)}
        />
      )
    } else if (item.status === 'mitra_on_the_way') {
      return (
        <Button
          text='Mulai'
          type='large'
          onPress={onPressStart}
        />
      )
    } else if (item.status === 'start_service') {
      return (
        <Button
          text='Selesai'
          type='large'
          model='outline'
          onPress={onPressFinish}
        />
      )
    } else if (item.status === 'finish_service') {
      return (
        <Button
          text='Tagih'
          type='large'
          model='outline'
          onPress={() => navigate('Payment', { code })}
        />
      );
    } else if (item.status === 'pending_payment') {
      return (
        <Text
          size='s'
          weight='bold'
          color='green'
          align='right'
          text='Menunggu Konfirmasi'
        />
      );
    } else if (item.status === 'done') {
      return (
        <Text
          size='s'
          weight='bold'
          color='green'
          text='Selesai'
        />
      );
    } else if (item.status === 'cancel') {
      return (
        <Text
          size='s'
          weight='bold'
          color='red'
          text='Batal'
        />
      );
    }
  }

  function momentParse(time: any, type: string = 'day') {
    return moment(time).utc().format(type === 'day' ? 'DD/MM/YYYY' : 'hh:mm');
  }

  return (
    <>
      <BoxShadow setting={shadowOpt}>
        <TouchableOpacity
          style={Style.container}
          activeOpacity={1}
          onPress={() => navigation.navigate('OrderDetails', { item, modalContent, onPressFinish, onPressStart, image: item.service.image, unassignOrder })}
        >
          <View style={Style.info}>
            <View>
              <Image source={{ uri: item.service.image }} style={Style.serviceImg} resizeMode='cover' resizeMethod='scale' />
            </View>
            <View style={{ flex: 1, paddingLeft: 10 }}>
              <Text
                size='s'
                weight='bold'
                color='black'
                mb={5}
                text={item?.service?.name}
              />
              <Text
                size='xs'
                weight='regular'
                color='darkBlueGrey'
                text={item?.code}
              />
            </View>
            <View style={{ alignItems: 'flex-end' }}>
              <Text
                size='xs'
                weight='bold'
                color={momentParse(item.work_time) === momentParse(Date.now()) ? 'yobenBlue' : 'darkBlueGrey'}
                mb={5}
                text={momentParse(item.work_time) === momentParse(Date.now()) ? 'Hari Ini' : momentParse(item.work_time)}
              />
              <Text
                size='xs'
                weight='bold'
                color={momentParse(item.work_time) === momentParse(Date.now()) ? 'yobenBlue' : 'darkBlueGrey'}
                text={momentParse(item.work_time, 'time')}
              />
            </View>
          </View>

          <View style={Style.address}>
            <View style={{ flex: 1 }}>
              <Text
                size='xs'
                weight='bold'
                color='black'
                mb={5}
                text={item.user.name}
              />
              <Text
                size='xs'
                weight='regular'
                color='darkBlueGrey'
                mb={5}
                numOfLines={1}
                text={item.address?.address}
              />
            </View>
            <View style={{ width: 120, alignItems: 'flex-end' }}>
              {renderButton(item.code)}
            </View>
          </View>
        </TouchableOpacity>
      </BoxShadow>

      <Modal
        ilu={modalContent.ilu}
        title={modalContent.title}
        desc={modalContent.desc}
        onClose={closeModal}
        open={modalVisible}
        leftBtn={{
          text: 'Batal',
        }}
        rightBtn={{
          text: modalContent.type === 'mulai' ? 'Mulai' : 'Ya',
          action: modalBtnRightAction
        }}
      />
    </>
  )
}

const Style = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    shadowOpacity: 0.5,
    shadowRadius: 10,
    shadowOffset: {
      height: 5,
      width: 0,
    },
    shadowColor: 'rgba(45, 104, 140, 0.1)',
    borderRadius: 10,
    height: normalize(155),
    justifyContent: 'center',
  },
  info: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: normalize(15),
    borderBottomWidth: 1,
    paddingBottom: normalize(15),
    borderBottomColor: '#F2F2F2'
  },
  address: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: normalize(15),
    paddingHorizontal: normalize(15),
  },
  serviceImg: {
    width: normalize(50),
    height: normalize(50),
    backgroundColor: colors.yobenBlue,
    borderRadius: 10
  }
})

export default OrderCard;