import React, { useEffect, useState } from 'react';
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { useNavigation } from '@react-navigation/native';

import { Button, Text, Drawer } from 'components';
import { normalize } from 'helpers';
import { colors, screenWidth, themes } from 'consts';
import { IcCamera } from 'svg';

interface ImageUploadProps {
  getImage?: (image: string) => void,
  paymentMethod?: string,
  retake?: boolean
}

const ImageUpload: React.FC<ImageUploadProps> = ({ getImage, paymentMethod, retake }) => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [imgUri, setImgUri] = useState('');
  const navigation = useNavigation();

  useEffect(() => {
    retake && setDrawerOpen(true);
  }, [retake])

  function openDrawer() {
    setDrawerOpen(true);
  }

  function closeDrawer() {
    setDrawerOpen(false);
  }

  function openCamera() {
    navigation.navigate('Camera', { previewImage })
    closeDrawer();
  }

  function previewImage(data: any) {
    setImgUri(data)
    getImage(data)
  }

  function openGallery() {
    ImagePicker.launchImageLibrary({
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      quality: 0.5,
      mediaType: 'photo',
      maxWidth: 1280,
      maxHeight: 720
    }, (response) => {
      if (response.didCancel || response.error) {
        closeDrawer()
      } else {
        setImgUri(response.uri)
        getImage(`data:image/png;base64,${response.data}`)
        // getImage(response.uri)
        closeDrawer()
      }
    });
  }

  return (
    <View>
      <TouchableOpacity
        onPress={openDrawer}
        activeOpacity={1}
        style={Style.container}
      >
        {
          imgUri && paymentMethod !== 'cash' ?
            <Image
              source={{ uri: imgUri }}
              resizeMode='contain'
              style={Style.imgPreview}
            />
            :
            <View style={{ alignItems: 'center' }}>
              <IcCamera />
              <Text
                size='s'
                color='yobenBlue'
                mt={normalize(10)}
                text='+ Upload Bukti Transfer'
              />
            </View>
        }
      </TouchableOpacity>

      <Drawer open={drawerOpen} close={closeDrawer}>
        <Button
          type='text'
          text='Pilih dari gallery'
          onPress={openGallery}
          style={Style.button}
        />
        <Button
          type='text'
          text='Ambil foto'
          onPress={openCamera}
          style={{...Style.button, borderWidth: 1, width: '100%', textAlign: 'center', alignItems: 'center'}}
        />
      </Drawer>
    </View>
  )
}

const Style = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: screenWidth / 2,
    borderWidth: 2,
    borderColor: colors.yobenBlue,
    borderRadius: 10,
    borderStyle: 'dashed',
  },

  button: {
    alignItems: 'center',
    justifyContent: 'center',
    height: normalize(50),
    width: '100%', 
    marginBottom: normalize(20),
    borderWidth: 1,
    borderRadius: normalize(10),
    borderColor: colors.yobenBlue,
  },

  imgPreview: {
    width: '100%',
    height: '100%'
  },
})
export default ImageUpload;