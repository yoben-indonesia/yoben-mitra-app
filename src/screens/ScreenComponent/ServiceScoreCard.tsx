import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import { BoxShadow } from 'react-native-shadow';

import { normalize, navigation } from 'helpers';
import { Text } from 'components';
import { colors, screenWidth } from 'consts';
import Language from 'language';

interface ServiceScoreProps {
}

const ServiceScore: React.FC<ServiceScoreProps> = () => {
  const shadowOpt = {
    width: Math.floor(screenWidth - normalize(40)),
    height: normalize(96),
    color: colors.darkBlue,
    border: 10,
    radius: 10,
    opacity: 0.05,
    x: 0,
    y: 5,
    style: { marginBottom: normalize(20) }
  }

  const renderText = () => (
    <View>
      <Text
        size='xs'
        weight='regular'
        color='darkBlueGrey'
        text={Language.Home.ServiceScore.yourScore}
      />
      <Text
        size='m'
        weight='bold'
        mt={normalize(8)}
        mb={normalize(8)}
        color='yobenBlue'
      >
        <IconAwesome name='star' size={normalize(16)} color={colors.yobenBlue} />  4.9
        </Text>
      <Text
        size='xs'
        weight='regular'
        color='black'
        text={Language.Home.ServiceScore.verySatisfy}
      />
    </View>
  );

  return (
    <BoxShadow setting={shadowOpt}>
      <TouchableOpacity style={Style.container} onPress={navigation('ServiceScore').navigate}>
        {renderText()}
        <Icon
          name='chevron-right'
          size={normalize(18)}
          color={colors.greyText}
        />
      </TouchableOpacity>
    </BoxShadow>
  );
}

const Style = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    shadowOpacity: 0.5,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 0,
    },
    height: normalize(100),
    shadowColor: 'rgba(45, 104, 140, 0.1)',
    backgroundColor: 'white',
    paddingHorizontal: normalize(20),
    borderRadius: 10,
    marginBottom: normalize(20)
  }
})

export default ServiceScore;