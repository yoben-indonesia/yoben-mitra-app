import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { BoxShadow } from 'react-native-shadow';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

import { currency, navigate, normalize } from 'helpers';
import { Text, Button } from 'components';
import { colors, screenWidth } from 'consts';
import { ballanceData, lazyLoadData } from 'selectors';

interface SaldoCarddProps { }

const SaldoCardd: React.FC<SaldoCarddProps> = () => {
  const ballance = useSelector(ballanceData).amount || 0;
  const isReady = useSelector(ballanceData).code
  const lazyLoad = useSelector(lazyLoadData)

  const renderBtn = () => {
    return ballance < 20000 ?
      <Button text='Top Up' onPress={() => navigate('TopUp')} /> :
      <Icon name='chevron-right' size={normalize(18)} color='#D0D0D0' />
  }

  function onPressCard() {
    navigate('Balance', { ballance })
  }

  const renderPlaceHolder = () => (
    <View style={{ alignItems: 'center', marginBottom: normalize(20) }}>
      <SkeletonPlaceholder backgroundColor='#ebebeb' highlightColor='#fcfcfc'>
        <SkeletonPlaceholder.Item width={screenWidth - normalize(40)} height={normalize(96)} borderRadius={10} />
      </SkeletonPlaceholder>
    </View>
  );

  const renderText = () => (
    <View>
      <Text
        size='xs'
        color='darkBlueGrey'
        weight='regular'
        text='Saldo Anda'
      />
      <Text
        size='m'
        weight='bold'
        color={ballance < 20000 ? 'red' : 'yobenBlue'}
        mt={8}
        mb={8}
      >
        <Icon name='wallet' size={normalize(18)} color={ballance < 20000 ? colors.red : colors.yobenBlue} />  {currency(ballance)}
      </Text>
      <Text
        size='xs'
        weight='regular'
        color={ballance < 20000 ? 'red' : 'black'}
        text={ballance < 20000 ? 'Top up untuk menerima order' : 'Anda masih bisa menerima order'}
      />
    </View>
  );

  const renderCard = () => {
    return (
      <BoxShadow setting={shadowOpt}>
        <TouchableOpacity
          onPress={onPressCard}
          activeOpacity={1}
          style={Style.container}
        >
          {renderText()}

          <View>
            {renderBtn()}
          </View>
        </TouchableOpacity>
      </BoxShadow>
    )
  }

  const renderContent = () => {
    if (lazyLoad && !isReady) {
      return renderPlaceHolder();
    }

    return renderCard();
  }

  return renderContent();
}

const shadowOpt = {
  width: Math.floor(screenWidth - normalize(40)),
  height: normalize(96),
  color: "#2D688C",
  border: 10,
  radius: 10,
  opacity: 0.05,
  x: 0,
  y: 5,
  style: { marginBottom: normalize(20) }
}

const Style = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    shadowOpacity: 0.5,
    shadowRadius: 5,
    shadowOffset: {
      height: 5,
      width: 0,
    },
    shadowColor: 'rgba(45, 104, 140, 0.1)',
    backgroundColor: 'white',
    paddingHorizontal: normalize(20),
    height: normalize(100),
    borderRadius: 10,
  }
})

export default SaldoCardd;