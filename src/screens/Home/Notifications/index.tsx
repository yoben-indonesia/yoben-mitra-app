import React from 'react';
import { View, StyleSheet } from 'react-native';

import { Text, Container } from 'components';
import { normalize } from 'helpers';
import { colors, themes } from 'consts';

const Notifications = () => {
  const [isMounted, setIsMounted] = React.useState(false);

  React.useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  return isMounted && (
    <Container style={{ paddingBottom: normalize(20) }}>
      <View style={Style.itemContainer}>
        <View style={Style.dots} />
        <View>
          <Text
            size='s'
            color='black'
            weight='regular'
            mb={10}
            text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. In non nullam eget massa morbi porttitor luctus.'
          />
          <Text
            size='xs'
            color='greyText'
            weight='regular'
            text='Monday, 01 January 2020'
          />
        </View>
      </View>

      <View style={Style.itemContainer}>
        <View style={Style.dots} />
        <View>
          <Text
            size='s'
            color='black'
            weight='regular'
            mb={10}
            text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. In non nullam eget massa morbi porttitor luctus.'
          />
          <Text
            size='xs'
            color='greyText'
            weight='regular'
            text='Monday, 01 January 2020'
          />
        </View>
      </View>

      <View style={Style.itemContainer}>
        <View style={[Style.dots, Style.read]} />
        <View>
          <Text
            size='s'
            color='black'
            weight='regular'
            mb={10}
            text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. In non nullam eget massa morbi porttitor luctus.'
          />
          <Text
            size='xs'
            color='greyText'
            weight='regular'
            text='Monday, 01 January 2020'
          />
        </View>
      </View>
    </Container>
  );
}

const Style = StyleSheet.create({
  itemContainer: {
    padding: normalize(20),
    flexDirection: 'row',
    ...themes.borderBottom
  },
  dots: {
    width: 8,
    height: 8,
    marginTop: normalize(5),
    marginRight: normalize(10),
    borderRadius: 50,
    backgroundColor: colors.yobenBlue,
  },

  read: {
    backgroundColor: colors.inputBorder,
  }
})

export default Notifications;