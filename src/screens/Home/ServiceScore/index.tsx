import React from 'react';
import { View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import { Container, Text } from 'components';
import { normalize } from 'helpers';
import { colors } from 'consts';
import Language from 'language';
const {
  yourScore, aboutScoreTitle, verySatisfy, satisfy, sufficient,
  lessSatisfactory, veryUnsatisfactory, desc1, desc2, starsList
} = Language.Home.ServiceScore;

const ServiceScore = (props) => {
  const [isMounted, setIsMounted] = React.useState(false);

  React.useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  const renderStarList = () => (
    starsList.map((star, index) => (
      <View key={index} style={Style.starItem}>
        <Icon name='star' color={colors.yobenBlue} size={normalize(18)} />
        <View style={Style.starScore}>
          <Text
            size='m'
            color='yobenBlue'
            text={star.star}
          />
        </View>
        <Text
          size='s'
          weight='regular'
          color='yobenBlue'
          text={star.desc}
        />
      </View>
    ))
  );

  return isMounted && (
    <Container>
      <View style={Style.title}>
        <Text
          size='xs'
          weight='regular'
          color='darkBlueGrey'
          text={yourScore}
        />
        <Text
          size='m'
          weight='bold'
          mt={normalize(8)}
          mb={normalize(8)}
          color='yobenBlue'
        >
          <Icon name='star' size={normalize(16)} color={colors.yobenBlue} />  4.9
        </Text>
        <Text
          size='xs'
          weight='regular'
          color='black'
          text={verySatisfy}
        />
      </View>

      <View style={{ padding: normalize(20) }}>
        <Text
          size='m'
          color='black'
          mb={normalize(15)}
          text={aboutScoreTitle}
        />

        <View style={Style.starContainer}>
          {renderStarList()}
        </View>

        <Text
          size='s'
          weight='regular'
          color='black'
          mb={normalize(10)}
          style={{ lineHeight: normalize(20) }}
          text={desc1}
        />
        <Text
          size='s'
          weight='regular'
          color='black'
          style={{ lineHeight: normalize(20) }}
          text={desc2}
        />
      </View>
    </Container>
  )
}

const Style = StyleSheet.create({
  title: {
    padding: normalize(20),
    borderBottomWidth: 1,
    borderColor: '#F2F2F2'
  },
  starContainer: {
    backgroundColor: colors.yobenBlueTransparent,
    borderRadius: 10,
    padding: normalize(20),
    marginBottom: normalize(15)
  },
  starItem: {
    flexDirection: 'row',
    marginBottom: normalize(5),
    alignItems: 'center'
  },
  starScore: {
    width: normalize(30),
    height: normalize(30),
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default ServiceScore;