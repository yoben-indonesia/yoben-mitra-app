import React from 'react';
import { View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { Container, Text } from 'components';
import { normalize, currency } from 'helpers';
import { colors } from 'consts';
import Language from 'language';
const {
  title, titleDesc, minBallance, minAmount, aboutBallanceTitle,
  balanceAdd, balanceAdd1, balanceAdd2, balanceAddDesc,
  balanceReduce, balanceReduceDesc, balanceReduce1
} = Language.Home.InfoBalance;
interface InfoBalanceProps { }

const InfoBalance: React.FC<InfoBalanceProps> = props => {
  const [isMounted, setIsMounted] = React.useState(false)

  React.useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  const renderText = (size: string, color: string, mb: number, text: string, weight: string = 'bold') => (
    <Text
      size={size}
      color={color}
      mb={normalize(mb)}
      text={text}
      weight={weight}
    />
  );

  return isMounted && (
    <Container style={{ padding: normalize(20) }}>
      {renderText('m', 'black', 15, title)}
      <View style={Style.minSaldoContainer}>
        <Text size='l' color='yobenBlue'>
          {minBallance} = <Icon name='wallet' size={20} /> {currency(minAmount)}
        </Text>
      </View>
      {renderText('s', 'black', 20, titleDesc)}
      {renderText('m', 'black', 20, aboutBallanceTitle)}
      {renderText('s', 'yobenBlue', 10, balanceAdd)}
      {renderText('s', 'black', 15, balanceAddDesc, 'regular')}
      {renderText('s', 'black', 15, balanceAdd1, 'regular')}
      {renderText('s', 'black', 20, balanceAdd2, 'regular')}
      {renderText('s', 'red', 10, balanceReduce)}
      {renderText('s', 'black', 15, balanceReduceDesc, 'regular')}
      {renderText('s', 'black', 15, balanceReduce1, 'regular')}
    </Container>
  );
}

const Style = StyleSheet.create({
  minSaldoContainer: {
    backgroundColor: colors.yobenBlueTransparent,
    alignItems: 'center',
    borderRadius: 10,
    paddingVertical: normalize(15),
    marginBottom: normalize(15)
  }
})

export default InfoBalance;