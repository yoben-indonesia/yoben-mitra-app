import React, { useState, useEffect, useCallback } from 'react';
import { 
  View, 
  Image, 
  Switch, 
  FlatList, 
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Messaging from '@react-native-firebase/messaging';
import { useFocusEffect } from '@react-navigation/native';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { BoxShadow } from 'react-native-shadow';
import moment from 'moment';

import { 
  getBalanceSummary,
  updateFcmToken,
  updateAcceptOrder,
  setAcceptOrder,
  getUnassignedOrders 
} from 'stores/action';
import { Button, Container, Text } from 'components';
import { normalize, navigate } from 'helpers';
import { IcTopUp, IluRefresh } from 'svg';
import { colors, images, screenWidth, themes } from 'consts';
import { 
  ballanceData,
  ordersData,
  mitraData,
  lazyLoadData,
  fcmToken,
  isAcceptOrder,
  unassignOrdersData,
  isLoading 
} from 'selectors';
import Language from 'language';

import {
  SaldoCard,
  ServiceScoreCard,
  OrderCard,
  NoOrder
} from '../../ScreenComponent';

interface IndexProps {
  navigation: any
}

const Index: React.FC<IndexProps> = ({ navigation }) => {
  const dispatch = useDispatch();
  const ballance = useSelector(ballanceData);
  const fcm_token = useSelector(fcmToken);
  const mitra = useSelector(mitraData);
  const lazyLoad = useSelector(lazyLoadData);
  const acceptOrder = useSelector(isAcceptOrder);
  const unassignOrders = useSelector(unassignOrdersData);
  const loading = useSelector(isLoading);
  const [refreshing, setRefreshing] = useState(false);
  const [isMounted, setIsMounted] = useState(false);

  // const toggleSwitch = () => setIsEnabled(previousState => !previousState);

  useEffect(() => {
    setIsMounted(true);
    fcm_token && dispatch(updateFcmToken({ fcm_token }));

    Messaging().setBackgroundMessageHandler(async remoteMessage => {
      const data = remoteMessage.data;
      data?.path && navigate(data.path);
    });
  }, [isMounted])

  useFocusEffect(
    useCallback(() => {
      dispatch(getBalanceSummary());
      acceptOrder && dispatch(getUnassignedOrders());
    }, [])
  );

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    dispatch(getUnassignedOrders(() => setRefreshing(false)))
  }, []);

  const toggleSwitch = () => {
    dispatch(setAcceptOrder(!acceptOrder))
    dispatch(updateAcceptOrder({is_accept_order: !acceptOrder}, () => dispatch(getUnassignedOrders())))
  }

  function onPressNavigate(routeName: string) {
    return () => navigation.navigate(routeName)
  }

  const renderLowBalance = () => (
    <View style={Style.emptyStateContainer}>
      <View style={{ alignItems: 'center', width: normalize(screenWidth * 0.7) }}>
        <IcTopUp />
        <Text
          size='m'
          weight='bold'
          color='yobenBlue'
          align='center'
          mt={10}
          mb={10}
          text={Language.Home.Home.emptyStateTitle}
        />
        <Text
          size='s'
          weight='regular'
          color='darkBlueGrey'
          align='center'
          mb={15}
          text={Language.Home.Home.emptyStateDesc}
        />
        <View>
          <Button
            text={Language.Home.Home.emptyStateBtnText}
            onPress={onPressNavigate('TopUp')}
          />
        </View>
      </View>
    </View>
  );

  const renderNewOrders = () => {
    if (loading) {
      return (
        <>
          <View style={{ alignItems: 'center', marginBottom: normalize(20) }}>
            <SkeletonPlaceholder backgroundColor='#ebebeb' highlightColor='#fcfcfc'>
              <SkeletonPlaceholder.Item width={screenWidth - normalize(40)} height={normalize(150)} borderRadius={10} />
            </SkeletonPlaceholder>
          </View>
          <View style={{ alignItems: 'center' }}>
            <SkeletonPlaceholder backgroundColor='#ebebeb' highlightColor='#fcfcfc'>
              <SkeletonPlaceholder.Item width={screenWidth - normalize(40)} height={normalize(150)} borderRadius={10} />
            </SkeletonPlaceholder>
          </View>
        </>
      );
    } else if (unassignOrders?.length && acceptOrder) {
      return (
        <FlatList
          data={unassignOrders}
          renderItem={({ item }) => <OrderCard item={item} navigation={navigation} unassignOrder />}
          keyExtractor={item => item.code}
          showsVerticalScrollIndicator={false}
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
          contentContainerStyle={{ flex: 0, paddingHorizontal: normalize(20) }}
        />
      )
    }
    else if (!unassignOrders?.length && acceptOrder) return <NoOrder text={Language.MyOrder.emptyState} />;
    else return <NoOrder text={Language.MyOrder.activeEmptyState} />;
  }

  const renderBtmSection = () => {
    // if (ballance?.amount < 20000) return renderLowBalance();
    return renderNewOrders();
  }

  return isMounted && (
    <Container
      style={Style.container}
    >
      <View style={{ paddingHorizontal: normalize(20) }}>
        <View style={Style.introContainer}>
          <View>
            <Text
              size='s'
              weight='regular'
              text={Language.Home.Home.welcome}
            />
            <Text
              size='l'
              weight='bold'
              text={mitra.name}
            />
          </View>
          <View style={Style.avatar}>
            <Image
              source={{ uri: mitra.avatar }}
              style={Style.avatarImg}
            />
          </View>
        </View>

        <SaldoCard />

        {/* <ServiceScoreCard /> */}

        <View style={{ ...themes.rowCenterBetween, marginBottom: 10 }}>
          <Text
            size='m'
            weight='bold'
            color='black'
            text={Language.Home.Home.newestOrder}
          />

          <View style={themes.rowCenterBetween}>
            {
              acceptOrder ?
              <TouchableOpacity
                onPress={() => dispatch(getUnassignedOrders())}
                style={{marginRight: normalize(15)}}
              >
                <IluRefresh/> 
              </TouchableOpacity> : <></>
            }

            <Switch
              trackColor={{ false: colors.red, true: colors.yobenBlue }}
              thumbColor={colors.white}
              onValueChange={toggleSwitch}
              value={acceptOrder}
              disabled={loading}
            />
          </View>
        </View>

        <BoxShadow setting={shadowOpt}>
          <View style={{ ...themes.rowCenterBetween, ...Style.mitraStatus }}>
            <Text
              size='m'
              weight='bold'
              color='darkBlue'
              text='Status Mitra'
            />
            <Text
              size='m'
              weight='bold'
              color={acceptOrder ? 'green' : 'greyText'}
              text={acceptOrder ? 'Aktif' : 'Tidak Aktif'}
            />
          </View>
        </BoxShadow>
      </View>
       
      {renderBtmSection()}
    </Container>
  );
}

const shadowOpt = {
  width: Math.floor(screenWidth - normalize(40)),
  height: normalize(50),
  color: colors.darkBlue,
  border: 10,
  radius: 10,
  opacity: 0.05,
  x: 0,
  y: 5,
  style: { marginBottom: normalize(20) }
}

const Style: any = {
  container: {
    backgroundColor: colors.background,
    paddingTop: normalize(20),
  },

  introContainer: {
    ...themes.rowCenterBetween,
    marginBottom: normalize(20),
  },

  avatar: {
    width: normalize(44),
    height: normalize(44),
    backgroundColor: colors.yobenBlue,
    borderRadius: 100,
    borderWidth: 4,
    borderColor: colors.white
  },
  avatarImg: {
    width: '100%',
    height: '100%',
    borderRadius: 100,
  },

  emptyStateContainer: {
    ...themes.columnCenter,
    paddingVertical: normalize(40),
    marginTop: normalize(-20)
  },

  mitraStatus: {
    paddingHorizontal: 15,
    backgroundColor: colors.white,
    shadowOpacity: 0.5,
    shadowRadius: 1,
    shadowOffset: {
      height: 2,
      width: 0,
    },
    shadowColor: 'rgba(45, 104, 140, 0.1)',
    borderRadius: 10,
    height: normalize(50),
  }
};

export default Index;