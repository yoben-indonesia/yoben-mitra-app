import React, { useState, useEffect } from 'react';
import { View, Linking } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { postTopUp } from 'stores/action';
import { Button, Container, Loading, Text } from 'components';
import { normalize, navigation } from 'helpers';
import Language from 'language';
import { isLoading } from 'selectors';
import { ImageUploader } from '../../ScreenComponent';

interface TopUpProps { }

const TopUp: React.FC<TopUpProps> = () => {
  const dispatch = useDispatch();
  const [img, setImg] = useState('');
  const loading = useSelector(isLoading);
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
  }, [isMounted])

  function handleDisabledTopUpBtn() {
    return !!(img !== '');
  }

  function onPressTopUpSaldo() {
    dispatch(postTopUp({
      receipt_image: img
    }))
  }

  function getImage(image: string) {
    setImg(image);
  }

  function onPressHelp() {
    Linking.openURL('whatsapp://send?text=Hi Admin Yoben, Saya ingin bertanya tentang Top Up Saldo&phone=6283807702018');
  }

  return isMounted && (
    <Container style={{ padding: normalize(20) }}>
      <Text
        size='m'
        color='black'
        mb={normalize(15)}
        text='Upload Bukti Transfer'
      />
      <ImageUploader
        getImage={getImage}
      />

      <View style={{ flex: 1 }} />
      <Button
        mb={normalize(20)}
        type='large'
        text='Top Up Saldo'
        onPress={onPressTopUpSaldo}
        disabled={!handleDisabledTopUpBtn()}
      />
      <Button
        mb={normalize(20)}
        type='large'
        model='outline'
        text='Bantuan'
        onPress={onPressHelp}
      />
      <Loading backdrop />
    </Container>
  )
}

export default TopUp;