import React from 'react';
import { View, FlatList, StyleSheet, Image } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';

import { getHistoryTrans } from 'stores/action';
import { Button, Container, Text } from 'components';
import { currency, normalize } from 'helpers';
import { colors, images, themes } from 'consts';
import { IcWallet } from 'svg';
import Language from 'language';
import { historyTransData, isLoading } from 'selectors';

interface BalanceProps {
  navigation: any,
  route: any,
}

const Balance: React.FC<BalanceProps> = ({ navigation, route: { params: {ballance} } }) => {
  const dispatch = useDispatch();
  const history = useSelector(historyTransData);
  const loading = useSelector(isLoading);
  const [isMounted, setIsMounted] = React.useState(false);

  React.useEffect(() => {
    setIsMounted(true);
    dispatch(getHistoryTrans());
  }, [isMounted])

  function statusColor(status: string) {
    return status === 'pending' ? 'darkBlue' : status === 'done' ? 'green' : 'red'
  }

  const renderItem = ({ item }) => {
    return (
      <View style={Style.itemContainer}>
        <View>
          <Text
            size='s'
            weight='regular'
            color='black'
            mb={normalize(5)}
            text={item.name}
          />
          <Text
            size='xs'
            weight='regular'
            color='darkBlueGrey'
            text={moment(item.u_at).format('DD/MM/yyy')}
          />
        </View>
        <View>
          <Text
            size='s'
            mb={normalize(5)}
            color={item.type === 'deposit' ? 'yobenBlue' : 'red'}
            text={`${item.type === 'deposit' ? '+' : '-'} ${currency(item.amount)}`}
          />
          <Text
            size='s'
            weight='regular'
            color={statusColor(item.status)}
            align='right'
            text={item.status}
          />
        </View>
      </View>
    );
  }

  return isMounted && (
    <Container>
      <View style={themes.borderBottom}>
        <View style={Style.titleTextContainer}>
          <View>
            <Text
              size='xs'
              color='darkBlueGrey'
              weight='regular'
              text={Language.Home.Balance.cardTitle}
            />
            <View style={themes.rowCenter}>
              <IcWallet
                fill={ballance < 20000 ? colors.red : colors.yobenBlue}
                width={normalize(24)}
                height={normalize(24)}
              />

              <Text
                size='m'
                weight='bold'
                color={ballance < 20000 ? 'red' : 'yobenBlue'}
                mt={normalize(10)}
                mb={normalize(10)}
                ml={normalize(10)}
                text={currency(ballance)}
              />
            </View>
            <Text
              size='xs'
              weight='regular'
              color={ballance < 20000 ? 'red' : 'black'}
              text={ballance < 20000 ? Language.Home.Balance.invalidStatus : Language.Home.Balance.validStatus}
            />
          </View>
          <View>
            <Button
              text={Language.Home.Balance.btnText}
              model={ballance < 20000 && 'outline'}
              onPress={() => navigation.navigate('TopUp')}
            />
          </View>
        </View>
      </View>

      <View style={Style.listItemContainer}>
        <Text color='black' size='m' mb={normalize(10)} text={Language.Home.Balance.listTitle} />
        {
          loading ?
            <View style={{ ...themes.columnCenter, height: '100%' }}>
              <Image source={images.EllipsisLoading} style={{ width: 50, height: 50 }} />
            </View>
            :
            <FlatList
              data={history}
              renderItem={renderItem}
              keyExtractor={item => item.code}
              bounces={false}
              showsVerticalScrollIndicator={false}
            />
        }
      </View>
    </Container>
  );
}

const Style = StyleSheet.create({
  titleTextContainer: {
    ...themes.rowCenterBetween,
    padding: normalize(20)
  },
  listItemContainer: {
    flex: -1,
    padding: normalize(20)
  },
  itemContainer: {
    ...themes.rowCenterBetween,
    ...themes.borderBottom,
    paddingVertical: normalize(15),
  }
})

const data: any = [
  {
    id: 1,
    date: '01/01/2020',
    title: 'Cash',
    amount: '- Rp 50,000'
  },
  {
    id: 2,
    date: '01/01/2020',
    title: 'Deposit',
    amount: '+ Rp 100,000'
  },
  {
    id: 3,
    date: '01/01/2020',
    title: 'Cash',
    amount: '- Rp 50,000'
  },
  {
    id: 4,
    date: '01/01/2020',
    title: 'Deposit',
    amount: '+ Rp 100,000'
  },
  {
    id: 5,
    date: '01/01/2020',
    title: 'Cash',
    amount: '- Rp 50,000'
  },
  {
    id: 6,
    date: '01/01/2020',
    title: 'Deposit',
    amount: '+ Rp 100,000'
  },
  {
    id: 7,
    date: '01/01/2020',
    title: 'Cash',
    amount: '- Rp 50,000'
  },
  {
    id: 8,
    date: '01/01/2020',
    title: 'Deposit',
    amount: '+ Rp 100,000'
  },
  {
    id: 9,
    date: '01/01/2020',
    title: 'Cash',
    amount: '- Rp 50,000'
  },
  {
    id: 10,
    date: '01/01/2020',
    title: 'Deposit',
    amount: '+ Rp 100,000'
  },
  {
    id: 11,
    date: '01/01/2020',
    title: 'Cash',
    amount: '- Rp 50,000'
  },
  {
    id: 12,
    date: '01/01/2020',
    title: 'Deposit',
    amount: '+ Rp 100,000'
  },
];

export default Balance;