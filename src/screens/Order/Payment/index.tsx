import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { BoxShadow } from 'react-native-shadow';
import { useSelector, useDispatch } from 'react-redux';

import { getAllPayment, getOrderDetail, postOrderPayment } from 'stores/action';
import { Button, Text, Container } from 'components';
import { normalize, responsive, navigation, currency, resetNav } from 'helpers';
import { IcBroom } from 'svg';
import { colors, screenWidth, themes } from 'consts';
import Language from 'language';
import { orderDetailData, paymentData, isLoading } from 'selectors';
import { ImageUploader } from '../../ScreenComponent';

interface OrderHistoryProps {
  route: any
}

const OrderHistory: React.FC<OrderHistoryProps> = ({ route: { params } }) => {
  const dispatch = useDispatch();
  const data = useSelector(orderDetailData);
  const paymentDatas = useSelector(paymentData);
  const loading = useSelector(isLoading);
  const [isMounted, setIsMounted] = useState(false);
  const [paymentMethod, setPaymentMethod] = useState('');
  const [paymentCode, setPaymentCode] = useState('');
  const [img, setImg] = useState('-');
  // const [img, setImg] = useState('');
  const [retake, setRetake] = useState(false);

  useEffect(() => {
    setIsMounted(true);
    dispatch(getAllPayment());
    dispatch(getOrderDetail(params?.code));
  }, [isMounted]);

  function parseDuration(duration: number) {
    if (duration / 3600 < 1) return `30 menit`;
    return `${duration / 3600} jam`
  }

  function getImage(image: string) {
    setImg(image);
    setPaymentMethod('transfer');
    const transfer = paymentDatas.filter(data => data.type === 'bank_transfer');
    setPaymentCode(transfer[0].code);
  }

  function onSelectCashMethod(code: string) {
    return () => {
      setImg('-');
      setPaymentMethod('cash');
      setPaymentCode(code);
    }
  }

  function onPressDone() {
    dispatch(
      postOrderPayment(
        {
          order_code: params.code,
          payment_code: paymentCode,
          receipt_image: paymentMethod === 'cash' ? '' : img,
        },
        () => resetNav('Success', {
          title: Language.Success.Payment.title,
          desc: Language.Success.Payment.desc
        })
      )
    );
  }

  const renderDiscount = () => {
    if (data?.price?.discount > 0) {
      return (
        <View style={Style.summaryItemContainer}>
          <Text
            size='xs'
            weight='regular'
            color='darkBlueGrey'
            text='Diskon'
          />
          <Text
            size='xs'
            weight='regular'
            color='red'
            text={`- ${currency(data?.price?.discount)}`}
          />
        </View>
      );
    };

    return <></>
  }

  const renderPaymentMethod = () => {
    return paymentDatas.map((data, index) => {
      if (data.type === 'cash') {
        return (
          <TouchableOpacity
            key={index}
            activeOpacity={1}
            style={Style.cashContainer}
            onPress={onSelectCashMethod(data?.code)}
          >
            <View style={{ ...Style.bulletPoints, borderWidth: paymentMethod === 'cash' ? responsive(normalize(8), normalize(6)) : 1 }} />
            <Text
              size='s'
              color='black'
              weight='regular'
              ml={normalize(10)}
              text={Language.Payment.cash}
            />
          </TouchableOpacity>
        )
      } else if (data.type === 'bank_transfer') {
        return (
          <View
            key={index}
            style={Style.transferContainer}
          >
            <View style={{ ...Style.bulletPoints, borderWidth: paymentMethod === 'transfer' ? responsive(normalize(8), normalize(6)) : 1 }} />
            <View style={Style.uploadContainer}>
              <View style={themes.rowCenterBetween}>
                <Text
                  size='s'
                  color='black'
                  weight='regular'
                  mb={normalize(15)}
                  text={Language.Payment.bank}
                />

                {
                  img ?
                    <Button
                      type='text'
                      text='Foto Ulang'
                      mb={normalize(15)}
                      onPress={() => setRetake(true)}
                    />
                    : <></>
                }
              </View>

              <ImageUploader
                getImage={getImage}
                paymentMethod={paymentMethod}
                retake={retake}
              />
            </View>
          </View>
        )
      }
    })
  }

  return isMounted && (
    <View style={Style.container}>
      <Container scroll style={{ paddingBottom: responsive(normalize(20), 0) }}>
        <View style={Style.packetContainer}>
          <View>
            <IcBroom
              width={normalize(20)}
              height={normalize(30)}
            />
          </View>
          <View style={{ paddingLeft: normalize(10) }}>
            <Text
              size='s'
              weight='bold'
              color='black'
              mb={5}
              text={data?.service?.name}
            />
            <Text
              size='xs'
              weight='regular'
              color='darkBlueGrey'
              text={`Durasi, ${parseDuration(data?.duration?.duration || 0)}`}
            />
          </View>
        </View>

        <View style={Style.summaryContainer}>
          <Text
            size='m'
            mb={15}
            color='black'
            text='Ringkasan'
          />
          <View style={Style.summaryItemContainer}>
            <Text
              size='xs'
              weight='regular'
              color='darkBlueGrey'
              text='Paket Reguler'
            />
            <Text
              size='xs'
              weight='regular'
              color='black'
              text={currency(data?.price?.service)}
            />
          </View>

          {
            data?.price?.service_tax > 0 ?
            <View style={Style.summaryItemContainer}>
              <Text
                size='xs'
                weight='regular'
                color='darkBlueGrey'
                text={Language.Payment.serviceFee}
              />
              <Text
                size='xs'
                weight='regular'
                color='black'
                text={currency(data?.price?.service_tax || 0)}
              />
            </View> : <></>
          }

          {renderDiscount()}

          <View style={{ ...Style.summaryItemContainer, marginBottom: 0 }}>
            <Text
              size='m'
              color='black'
              text={Language.Payment.grandTotal}
            />
            <Text
              size='m'
              color='yobenBlue'
              text={currency(data?.price?.total || 0)}
            />
          </View>

        </View>

        <View style={Style.paymentContainer}>
          <Text
            size='m'
            color='black'
            text={Language.Payment.paymentMethodTitle}
          />
          {renderPaymentMethod()}
        </View>
      </Container>

      <BoxShadow setting={shadowConfig}>
        <View style={Style.btnContainer}>
          <Button
            text={Language.Payment.finish}
            disabled={!paymentMethod}
            type='large'
            loading={loading}
            onPress={onPressDone}
          />
        </View>
      </BoxShadow>
    </View>
  );
}

const shadowConfig = {
  width: screenWidth,
  height: normalize(75),
  color: colors.darkBlue,
  border: 10,
  radius: 5,
  opacity: 0.03,
  x: 0,
  y: 0,
}

const Style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  packetContainer: {
    flexDirection: 'row',
    padding: normalize(20),
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2'
  },
  summaryContainer: {
    padding: normalize(20),
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2'
  },
  summaryItemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: normalize(15)
  },
  paymentContainer: {
    flex: 1,
    padding: normalize(20),
    // paddingBottom: normalize(100)
  },

  cashContainer: {
    ...themes.rowCenter,
    ...themes.borderBottom,
    paddingVertical: normalize(20)
  },
  transferContainer: {
    flexDirection: 'row',
    paddingVertical: normalize(20)
  },
  uploadContainer: {
    flex: 1,
    marginLeft: normalize(10),
  },
  bulletPoints: {
    width: normalize(20),
    height: normalize(20),
    borderColor: colors.yobenBlue,
    borderRadius: 50,
  },

  btnContainer: {
    position: 'absolute',
    bottom: 0,
    width: screenWidth,
    paddingHorizontal: normalize(20),
    justifyContent: 'center',
    height: normalize(75),
    backgroundColor: colors.white,
    shadowOpacity: 0.5,
    shadowRadius: 10,
    shadowOffset: {
      height: normalize(-5),
      width: 0,
    },
    shadowColor: 'rgba(45, 104, 140, 0.1)',
  }
})

export default OrderHistory;