import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { useFocusEffect } from '@react-navigation/native';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

import { getOrdersDone, getOrdersAnalytics } from 'stores/action';
import { normalize, platformScale } from 'helpers';
import { Container, Text } from 'components';
import { colors, screenWidth } from 'consts';
import Language from 'language';
import { ordersDoneData, ordersAnalyticsData, lazyLoadData } from 'selectors';
import { OrderCard, NoOrder } from '../../ScreenComponent'

interface OrderHistoryProps {
  navigation: any
}

const OrderHistory: React.FC<OrderHistoryProps> = ({ navigation }) => {
  const dispatch = useDispatch();
  const orders = useSelector(ordersDoneData);
  const analytics = useSelector(ordersAnalyticsData);
  const lazyLoad = useSelector(lazyLoadData);
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
    return () => { setIsMounted(false) };
  }, [isMounted]);

  useFocusEffect(
    React.useCallback(() => {
      dispatch(getOrdersDone());
      dispatch(getOrdersAnalytics());
    }, [])
  );

  const renderPlaceHolder = () => (
    <View style={{ alignItems: 'center' }}>
      <SkeletonPlaceholder backgroundColor='#ebebeb' highlightColor='#fcfcfc'>
        <SkeletonPlaceholder.Item width={screenWidth - normalize(40)} height={normalize(155)} borderRadius={10} marginBottom={normalize(20)} />
      </SkeletonPlaceholder>
      <SkeletonPlaceholder backgroundColor='#ebebeb' highlightColor='#fcfcfc'>
        <SkeletonPlaceholder.Item width={screenWidth - normalize(40)} height={normalize(155)} borderRadius={10} marginBottom={normalize(20)} />
      </SkeletonPlaceholder>
    </View>
  );

  const renderOrderList = () => orders.map((item, index) => <OrderCard key={index} item={item} navigation={navigation} />);

  const renderText = (text: string, size: string = 'xs') => (
    <Text
      size={size}
      color='white'
      align='center'
      mb={size === 'xs' ? 5 : 0}
      text={text}
    />
  );

  const renderContent = () => {
    if (lazyLoad.ordersDone && !orders?.length) {
      return renderPlaceHolder();
    }

    if (orders?.length) {
      return renderOrderList();
    }

    return <NoOrder text={Language.HistoryOrder.emptyState} />
  };

  return isMounted && (
    <Container scroll={orders?.length ? true : false} style={Style.container}>
      <View style={Style.analyticsContainer}>
        <View style={{ flex: 1, }}>
          {renderText(Language.HistoryOrder.today)}
          {renderText(analytics.today, 'l')}
        </View>
        <View style={Style.thisWeek}>
          {renderText(Language.HistoryOrder.thisWeek)}
          {renderText(analytics.thisWeek, 'l')}
        </View>
        <View style={{ flex: 1, }}>
          {renderText(Language.HistoryOrder.thisMonth)}
          {renderText(analytics.thisMonth, 'l')}
        </View>
      </View>

      {renderContent()}
    </Container>
  )
}

const Style = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginTop: 0,
    backgroundColor: colors.background,
    paddingTop: platformScale(20, 0)
  },

  analyticsContainer: {
    width: screenWidth - normalize(40),
    backgroundColor: colors.darkBlue,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 10,
    borderRadius: normalize(10),
    marginBottom: normalize(20),
    marginTop: platformScale(0, 20)
  },

  thisWeek: {
    flex: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: 'rgba(255,255,255,0.3)'
  }
})

export default OrderHistory;