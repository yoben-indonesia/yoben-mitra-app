import React, { useState, useEffect } from 'react';
import { StyleSheet, FlatList, View } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { useFocusEffect } from '@react-navigation/native';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

import { getOrders } from 'stores/action';
import { normalize } from 'helpers';
import { Container } from 'components';
import { colors, screenWidth } from 'consts';
import { ordersData, lazyLoadData } from 'selectors';
import Language from 'language';

import { OrderCard, NoOrder } from '../../ScreenComponent'

interface IndexProps {
  navigation: any
}

const Index: React.FC<IndexProps> = ({ navigation }) => {
  const dispatch = useDispatch();
  const orders = useSelector(ordersData);
  const lazyLoad = useSelector(lazyLoadData);
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);

    return () => { setIsMounted(false) }
  }, [isMounted]);

  useFocusEffect(
    React.useCallback(() => {
      dispatch(getOrders());
    }, [])
  );

  const renderPlaceHolder = () => (
    <View style={{ alignItems: 'center', marginTop: normalize(20) }}>
      <SkeletonPlaceholder backgroundColor='#ebebeb' highlightColor='#fcfcfc'>
        <SkeletonPlaceholder.Item width={screenWidth - normalize(40)} height={normalize(155)} borderRadius={10} marginBottom={normalize(20)} />
      </SkeletonPlaceholder>
      <SkeletonPlaceholder backgroundColor='#ebebeb' highlightColor='#fcfcfc'>
        <SkeletonPlaceholder.Item width={screenWidth - normalize(40)} height={normalize(155)} borderRadius={10} marginBottom={normalize(20)} />
      </SkeletonPlaceholder>
      <SkeletonPlaceholder backgroundColor='#ebebeb' highlightColor='#fcfcfc'>
        <SkeletonPlaceholder.Item width={screenWidth - normalize(40)} height={normalize(155)} borderRadius={10} marginBottom={normalize(20)} />
      </SkeletonPlaceholder>
    </View>
  );

  const renderOrderList = () => {
    return (
      <FlatList
        data={orders}
        renderItem={renderItemList}
        keyExtractor={item => item.code}
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingHorizontal: normalize(20), marginTop: normalize(20), paddingBottom: normalize(20) }}
      />
    );
  };

  const renderItemList = ({ item }) => {
    return <OrderCard
      item={item}
      navigation={navigation}
    />
  };

  const renderContent = () => {
    if (lazyLoad.orders && !orders?.length) {
      return renderPlaceHolder();
    } else if (orders?.length) {
      return renderOrderList();
    } else return <NoOrder text={Language.MyOrder.emptyState} />
  }

  return isMounted && (
    <Container style={Style.container}>
      {renderContent()}
    </Container>
  );
}

const Style = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    marginTop: 0,
  }
})

export default Index;