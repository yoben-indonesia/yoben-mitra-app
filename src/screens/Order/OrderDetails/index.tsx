import React, { useState, useEffect, useMemo } from 'react';
import { View, StyleSheet, Image, ImageBackground } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import RNModal from 'react-native-modal';
import Icon from 'react-native-vector-icons/FontAwesome5';
import IconClose from 'react-native-vector-icons/MaterialCommunityIcons';
import CountDown from 'react-native-countdown-component';
import { BoxShadow } from 'react-native-shadow';
import moment from 'moment';

import { 
  getOrderDetail,
  getServiceDuration,
  updateDuration,
  getUnassignedDetail,
  postUnassignedOrders,
  updateStatus,
} from 'stores/action';
import { 
  Button,
  Container,
  Text,
  Pill,
  Loading 
} from 'components';
import { IluCancel, IluImportant, IluOk, IluSmile } from 'svg';
import { currency, navigate, normalize } from 'helpers';
import { colors, images, screenWidth, themes } from 'consts';
import Language from 'language';
import { orderDetailData, serviceDurationData, isLoading } from 'selectors';

interface OrderHistoryProps {
  route?: any,
}

const OrderHistory: React.FC<OrderHistoryProps> = ({ route: { params: { item, image, onPressFinish, onPressStart, unassignOrder } } }) => {
  const dispatch = useDispatch();
  const data = useSelector(orderDetailData);
  const loading = useSelector(isLoading);
  const serviceDuration = useSelector(serviceDurationData);
  const [isMounted, setIsMounted] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [modal, setModal] = useState({
    ok: false,
    order: false,
    failed: false,
    failedMsg: ''
  })
  const [duration, setDuration] = useState({
    duration: '',
    code: ''
  });
  const [showCountdown, setShowCountdown] = useState(false);

  useEffect(() => {
    setIsMounted(true);
    if(unassignOrder) dispatch(getUnassignedDetail(item?.code));
    else dispatch(getOrderDetail(item?.code, () => setShowCountdown(true)));
  }, []);

  useEffect(() => {
    dispatch(getServiceDuration(item?.service?.code));
  }, [item?.service?.code]);

  const countDown:number = useMemo(() => {
    let timeNow = moment(new Date());
    let timeStart = moment(data?.time?.start);
    let timeLeft = timeNow.diff(timeStart, 'seconds');
    let durationLeft = data?.duration?.duration - timeLeft;

    return durationLeft;
  }, [data?.duration?.duration])

  function onCloseModal() {
    setModalVisible(false);
    setModal({
      ok: false,
      order: false,
      failed: false,
      failedMsg: ''
    })
  }

  function parseDuration(duration: number) {
    if (duration / 3600 < 1) return `30 menit`;
    return `${duration / 3600} jam`
  }

  function parseStatus(status: string) {
    let text, color;
    switch (status) {
      case 'mitra_assigned': text = 'Order Diterima', color = 'darkBlue'; break;
      case 'mitra_on_the_way': text = 'Dalam Perjalanan', color = 'darkBlue'; break;
      case 'start_service': text = 'Sedang Bekerja', color = 'yobenBlue'; break;
      case 'finish_service': text = 'Selesai Bekerja', color = 'yobenBlue'; break;
      case 'pending_payment': text = 'Menunggu Konfirmasi', color = 'green'; break;
      case 'done': text = 'Selesai', color = 'green'; break;
      case 'cancel': text = 'Batal', color = 'red'; break;
    }

    return { text, color }
  }

  const onPressAddDuration = () => {
    dispatch(
      updateDuration(
        {
          order_code: item?.code,
          duration_code: duration.code
        },
        () => dispatch(getOrderDetail(item?.code)),
      )
    );
  }

  const onPressTakeOrder = () => {
    dispatch(postUnassignedOrders(item?.code, (data) => {
      data.code === 200 ? setModal({ok: true, failed: false, order: true, failedMsg: '', }) : setModal({ok: false, failed: true, order: true , failedMsg: data.message });
      dispatch(getOrderDetail(item?.code, () => setShowCountdown(true)));
      dispatch(getServiceDuration(item?.service?.code));
    }))
  }

  const onPressGo = () => {
    dispatch(
      updateStatus(
        {
          order_code: data?.code,
          status: 'mitra_on_the_way',
          mitra_code: data?.mitra?.code
        },
        () => dispatch(getOrderDetail(item?.code))
      )
    );
  }

  const renderButton = (status: string) => {
    let btnType = (btn) => (
      <BoxShadow setting={shadowConfig}>
        <View style={Style.btnContainer}>
          {btn}
        </View>
      </BoxShadow>
    )

    if (status === 'mitra_assigned') return btnType(<Button text='Berangkat' type='large' onPress={onPressGo}/>);
    else if (status === 'mitra_on_the_way') return btnType(<Button text={Language.Payment.start} type='large' onPress={onPressStart} />);
    else if (status === 'start_service') return btnType(<Button text={Language.Payment.finish} type='large' model='outline' onPress={onPressFinish} />);
    else if (status === 'finish_service') return btnType(<Button text={'Tagih'} type='large' model='outline' onPress={() => navigate('Payment', { code: item?.code })} />);
    else if (unassignOrder) return btnType(<Button text='Ambil Order' type='large' onPress={() => setModal({ok: false, failed: false, order: true, failedMsg: '' })} />);
    else return <></>;
  }

  const renderDiscount = () => {
    if (data?.price?.discount > 0) {
      return (
        <View style={Style.summaryList}>
          <Text
            size='xs'
            weight='regular'
            color='darkBlueGrey'
            text='Diskon'
          />
          <Text
            size='xs'
            weight='regular'
            color='red'
            text={`- ${currency(data?.price?.discount)}`}
          />
        </View>
      );
    };

    return <></>;
  }

  const renderCountdown = () => {
    if (item.status === 'start_service') {
      return (
        <View style={Style.countdownContainer}>
          <Text
            size='m'
            color='yobenBlue'
            text='Sisa Waktu Kerja Anda'
            align='center'
            mb={15}
          />
          {
            showCountdown && countDown ?
            <CountDown
              until={countDown}
              digitStyle={{ backgroundColor: colors.white, marginHorizontal: 10 }}
              digitTxtStyle={{ color: colors.black }}
              timeToShow={['H', 'M', 'S']}
              timeLabels={{ h: '', m: '', s: '' }}
              size={20}
              separatorStyle={{ color: colors.black }}
              showSeparator
            /> :
            <></>
          }
        </View>
      );
    };

    return <></>;
  }

  const renderAddDuration = () => {
    if (item.status === 'start_service' && serviceDuration.length) {
      const availDurations = serviceDuration?.filter(datum => {
        if (datum?.duration > data?.duration?.duration) return data;
      })

      if (availDurations.length) {
        return (
          <View>
            <Text
              size='m'
              color='black'
              mb={normalize(20)}
              text={'Ubah Total Durasi'}
            />
  
            <View style={{ flexDirection: 'row', marginBottom: 5, flexWrap: 'wrap' }}>
              {renderDurationPills(availDurations)}
            </View>
  
            <Button
              text='Ubah Durasi'
              type='large'
              loading={loading}
              onPress={onPressAddDuration}
              disabled={!duration.duration}
            />
          </View>
        );
      }

      return <></>
    };
  }

  const renderDurationPills = (availDurations) => {
    if (availDurations.length) {
      return availDurations.map((data, index) => (
        <Pill
          key={index}
          text={parseDuration(data.duration)}
          mr={15}
          mb={15}
          onPress={() => setDuration(data)}
          isSelected={data.code === duration.code}
        />
      ));
    };

    return <></>;
  }

  const renderPayment = () => {
    if (data?.transactions?.length) {
      let payment = data.transactions[0]
      return (
        <View style={{ borderTopWidth: 1, borderColor: colors.inputBorder, paddingTop: normalize(15) }}>
          <Text
            size='m'
            color='black'
            mb={15}
            text='Pembayaran'
          />
          <View style={Style.summaryList}>
            <Text
              color='darkBlueGrey'
              text='Status Pembayaran'
            />
            <Text
              color='black'
              text={payment.status}
            />
          </View>
          <View style={Style.summaryList}>
            <Text
              color='darkBlueGrey'
              text='Metode Pembayaran'
            />
            <Text
              color='black'
              text={payment.payment?.name}
            />
          </View>
          <View style={Style.summaryList}>
            <Text
              color='darkBlueGrey'
              text='Bukti Pembayaran'
            />
            <Button
              type='text'
              text='Lihat'
              onPress={() => setModalVisible(true)}
            />
          </View>

          <RNModal
            isVisible={modalVisible}
            backdropOpacity={0.4}
            swipeDirection={['down', 'up', 'left', 'right']}
            onBackdropPress={onCloseModal}
            onSwipeComplete={onCloseModal}
          >
            <View style={{ borderRadius: 10 }}>
              <ImageBackground
                source={{ uri: payment.receipt_image }}
                style={{ width: screenWidth - normalize(40), height: 500, position: 'relative', backgroundColor: colors.buttonShadow, borderRadius: 10 }}
                loadingIndicatorSource={images.InfinityLoading}
              >
                <View style={{ padding: 5, backgroundColor: 'rgba(255,255,255,0.2)', position: 'absolute', top: normalize(10), right: normalize(10), borderRadius: 100 }}>
                  <IconClose
                    name='close'
                    size={normalize(26)}
                    color={colors.white}
                    onPress={onCloseModal}
                  />
                </View>
              </ImageBackground>
            </View>
          </RNModal>

        </View>
      );
    }
  }

  const renderModalContent = () => {
    if (modal.ok) {
      return (
        <View style={{ borderRadius: 10, backgroundColor: 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', padding: normalize(20) }}>
          <IluOk width={100} height={100} />
          <Text size='m' color='yobenBlue' align='center' mt={20} mb={10}>Order Berhasil Diambil!</Text>
          <Text size='s' color='darkBlueGrey' weight='regular' align='center' mb={20}>Selamat Bekerja 😁</Text>
          <View style={Style.btnModalContainer}>
            <View style={{ flex: 1, marginRight: normalize(20) }}>
              <Button text='Tutup' model='outline' type='large' onPress={onCloseModal}></Button>
            </View>
          </View>
        </View>
      );
    } else if (modal.failed) {
      return (
        <View style={{ borderRadius: 10, backgroundColor: 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', padding: normalize(20) }}>
          <IluCancel width={100} height={100} />
          <Text size='m' color='red' align='center' mt={20} mb={10}>Order Gagal Diambil!</Text>
          <Text size='s' color='darkBlueGrey' weight='regular' align='center' mb={20}>{modal.failedMsg}</Text>
          <View style={Style.btnModalContainer}>
            <View style={{ flex: 1, marginRight: normalize(20) }}>
              <Button text='Tutup' model='outline-red' type='large' onPress={onCloseModal}></Button>
            </View>
          </View>
        </View>
      )
    } else if (modal.order) {
      return (
        <View style={{ borderRadius: 10, backgroundColor: 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', padding: normalize(20) }}>
              <IluImportant width={100} height={100} />
          <Text size='m' color='yobenBlue' align='center' mt={20} mb={10}>Ambil Order?</Text>
          <Text size='s' color='darkBlueGrey' weight='regular' align='center' mb={20}>Apakah kamu yakin untuk mengambil order ini?</Text>
          <View style={Style.btnModalContainer}>
            <View style={{ flex: 1, marginRight: normalize(20) }}>
              <Button text='Tutup' model='outline' type='large' onPress={onCloseModal}></Button>
            </View>
            <View style={{ flex: 1 }}>
              <Button text='Ambil' type='large' loading={loading} onPress={onPressTakeOrder}></Button>
            </View>
          </View>
        </View>
      );
    }
  }

  const renderContent = () => {
    return (
      <>
        <Container scroll>
          <View style={Style.packetContainer}>
            <View style={{ paddingLeft: normalize(10) }}>
              <Text
                weight='bold'
                color='black'
                mb={normalize(15)}
                text={data?.code}
              />
              <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                <Image source={{uri: image }} style={Style.serviceImg}/>
                <View>

              <Text
                weight='bold'
                color='black'
                mb={5}
                text={data?.service?.name}
              />
              <Text
                size='xs'
                weight='regular'
                color='darkBlueGrey'
                text={`Durasi, ${parseDuration(data?.duration?.duration || 0)}`}
              />
                </View>
              </View>
            </View>
            <View>
              <Text
                weight='bold'
                color={parseStatus(data.status).color}
                text={parseStatus(data.status).text}
              />
            </View>
          </View>

          <View style={Style.addressContainer}>
            <View style={Style.detailAddressContainer}>
              <Icon
                name='map-marker-alt'
                color={colors.yobenBlue}
                size={normalize(20)}
              />
              <View style={{ marginLeft: normalize(10) }}>
                <Text
                  size='s'
                  color='black'
                  weight='regular'
                  mb={normalize(10)}
                  text={data?.address?.name}
                />
                <Text
                  color='darkBlueGrey'
                  weight='regular'
                  mb={normalize(10)}
                  text={data?.address?.address}
                />
                <Text
                  color='black'
                  weight='regular'
                  mb={5}
                  text='Catatan:'
                />
                <Text
                  color='darkBlueGrey'
                  weight='regular'
                  mb={normalize(10)}
                  text={data?.address?.description}
                />
                <Text
                  color='black'
                  weight='regular'
                  mb={5}
                  text='No. Hp:'
                />
                <Text
                  color='darkBlueGrey'
                  weight='regular'
                  text={`+${data?.user?.phone}`}
                />
              </View>
            </View>

            <View style={{ ...themes.rowCenter }}>
              <Icon
                name='calendar-alt'
                color={colors.yobenBlue}
                size={normalize(20)}
              />
              <Text
                size='s'
                color='black'
                weight='regular'
                ml={normalize(10)}
                text={moment(data?.work_time).utc().format('dddd, DD MMMM YYYY - hh:mm')}
              />
            </View>
          </View>

          <View style={{ padding: normalize(20), paddingBottom: 60 }}>
            <Text
              size='m'
              mb={15}
              color='black'
              text={Language.Payment.summary}
            />
            <View style={Style.summaryList}>
              <Text
                size='xs'
                weight='regular'
                color='darkBlueGrey'
                text='Paket Reguler'
              />
              <Text
                size='xs'
                weight='regular'
                color='black'
                text={currency(data?.price?.service || 0)}
              />
            </View>

            {
              data?.price?.service_tax > 0 ?
              <View style={Style.summaryList}>
                <Text
                  size='xs'
                  weight='regular'
                  color='darkBlueGrey'
                  text={Language.Payment.serviceFee}
                />
                <Text
                  size='xs'
                  weight='regular'
                  color='black'
                  text={currency(data?.price?.service_tax)}
                />
              </View> : <></>
            }

            {renderDiscount()}

            <View style={Style.summaryList}>
              <Text
                size='m'
                color='black'
                text={Language.Payment.grandTotal}
              />
              <Text
                size='m'
                color='yobenBlue'
                text={currency(data?.price?.total || 0)}
              />
            </View>

            {renderCountdown()}

            {renderAddDuration()}

            {renderPayment()}

          </View>
        </Container>
        
        {renderButton(item.status)}

        <RNModal
          isVisible={modal.order}
          backdropOpacity={0.4}
          swipeDirection={['down', 'up', 'left', 'right']}
          onBackdropPress={onCloseModal}
          onSwipeComplete={onCloseModal}
        >
          {renderModalContent()}
        </RNModal>

        <Loading />
      </>
    );
  }

  return isMounted && renderContent();
}

const shadowConfig = {
  width: screenWidth,
  height: normalize(75),
  color: colors.darkBlue,
  border: 10,
  radius: 5,
  opacity: 0.03,
  x: 0,
  y: 0,
}

const Style = StyleSheet.create({
  packetContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: normalize(20),
    ...themes.borderBottom
  },

  addressContainer: {
    padding: normalize(20),
    ...themes.borderBottom
  },

  detailAddressContainer: {
    flexDirection: 'row',
    marginBottom: normalize(20)
  },

  summaryList: {
    ...themes.rowCenterBetween,
    marginBottom: normalize(15)
  },

  btnContainer: {
    position: 'absolute',
    bottom: 0,
    width: screenWidth,
    paddingHorizontal: normalize(20),
    justifyContent: 'center',
    height: normalize(75),
    backgroundColor: colors.white,
    shadowOpacity: 0.5,
    shadowRadius: 10,
    shadowOffset: {
      height: normalize(-5),
      width: 0,
    },
    shadowColor: 'rgba(45, 104, 140, 0.1)',
  },

  btnModalContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%'
  },

  serviceImg: {
    width: normalize(50),
    height: normalize(50),
    backgroundColor: colors.yobenBlue,
    borderRadius: 10,
    marginRight: 10
  },

  countdownContainer: {
    backgroundColor: colors.yobenBlueTransparent,
    borderRadius: 10,
    paddingVertical: 15,
    marginTop: 15,
    marginBottom: 20
  }
})

export default OrderHistory;