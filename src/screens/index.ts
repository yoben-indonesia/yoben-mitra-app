import Home from './Home/Home';
import Balance from './Home/Balance';
import InfoBalance from './Home/InfoBalance';
import ServiceScore from './Home/ServiceScore';
import TopUp from './Home/TopUp';
import Notifications from './Home/Notifications';

import CustPreferences from './Authentication/CustPreferences';
import Login from './Authentication/Login';
import Otp from './Authentication/OTP';
import CreatePassword from './Authentication/CreatePassword';
import ForgorPassword from './Authentication/ForgotPassword';

import MyOrder from './Order/MyOrder';
import OrderHistory from './Order/OrderHistory';
import OrderDetails from './Order/OrderDetails';
import Payment from './Order/Payment';

import MyProfile from './Profile/MyProfile';
import Setting from './Profile/Setting';
import TnC from './Profile/TnC';
import ChangePwd from './Profile/ChangePassword';
import Feedback from './Profile/Feedback';

import Camera from './Camera';
import Success from './Success';

export {
  //Auth Stack
  CustPreferences,
  Login,
  Otp,
  CreatePassword,
  ForgorPassword,
  //Home Stack
  Home,
  Balance,
  ServiceScore,
  InfoBalance,
  TopUp,
  Notifications,
  //Order Stack
  MyOrder,
  OrderHistory,
  OrderDetails,
  Payment,
  //Profile Stack
  MyProfile,
  Setting,
  TnC,
  ChangePwd,
  Feedback,
  //
  Camera,
  Success,
};
