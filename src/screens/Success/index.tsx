import React from 'react';
import { View, StyleSheet } from 'react-native';

import { Button, Text, Container } from 'components';
import { normalize } from 'helpers';
import Language from 'language';
import { IluOk } from 'svg';

interface SuccessProps {
  navigation?: any,
  route?: any,
}

const Success: React.FC<SuccessProps> = ({ navigation, route: { params } }) => {
  const [isMounted, setIsMounted] = React.useState(false);

  React.useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  function onPressBackHome() {
    navigation.replace('Dashboard')
  }

  return isMounted && (
    <Container style={Style.container}>
      <IluOk />
      <Text
        size='m'
        color='yobenBlue'
        align='center'
        mt={normalize(20)}
        mb={normalize(10)}
        text={params.title}
      />
      <Text
        size='s'
        color='darkBlueGrey'
        weight='regular'
        align='center'
        mb={normalize(20)}
        style={{ lineHeight: normalize(20) }}
        text={params.desc}
      />

      <View>
        <Button text={Language.Success.btnText} onPress={onPressBackHome}></Button>
      </View>
    </Container>
  );
}

const Style = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: normalize(25),
  },
})

export default Success;