import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import CountDown from 'react-native-countdown-component';

import { getOtp, changePwd, forgotPwd } from 'stores/action';
import { Button, Container, Text } from 'components';
import { normalize, navigate } from 'helpers';
import { colors, screenWidth } from 'consts';
import { otpCode, isLoading } from 'selectors';
import Language from 'language';
import GoBack from '../goBack';

interface OtpProps {
  route?: any
}

const Otp: React.FC<OtpProps> = ({ route: { params } }) => {
  const dispatch = useDispatch();
  const otp = useSelector(otpCode);
  const loading = useSelector(isLoading);
  const [isMounted, setIsMounted] = useState(false);
  const [resend, setResend] = useState(false);
  const [alertMsg, setAlertMsg] = useState('');

  let otpRef: any = React.useRef()

  useEffect(() => {
    setIsMounted(true);
    params.fromLogin && handleGetOtp();
  }, [isMounted]);

  function onPressResend() {
    if(params.fromChangePwd) {
      dispatch(changePwd({...params.data}));
    } else if (params.fromForgotPwd) {
      dispatch(forgotPwd({...params.data}));
    } else handleGetOtp();
  }

  function handleGetOtp() {
    dispatch(getOtp({ phone: params?.phone }, getOtpCb));
  }

  function getOtpCb() {
    setResend(false);
    setAlertMsg('');
    otpRef.current.state.digits = [];
    otpRef.current.blurAllFields();
    otpRef.current.focusField(0);
  }

  const renderResend = () => {
    if (resend) {
      return (
        <Button
          text={Language.Auth.otp.button}
          loading={loading}
          style={{ width: 120 }}
          activeOpacity={1}
          onPress={onPressResend}
        />
      );
    } else {
      return (
        <CountDown
          until={120}
          digitStyle={{ backgroundColor: colors.white }}
          digitTxtStyle={{ color: colors.yobenBlue, fontWeight: '800' }}
          timeToShow={['M', 'S']}
          timeLabels={{ m: '', s: '' }}
          size={16}
          separatorStyle={{ color: colors.yobenBlue, fontWeight: '400' }}
          showSeparator
          onFinish={() => setResend(true)}
        />
      );
    }
  }

  function onCodeFilled(code) {
    if(params.fromLogin) navigate('CreatePassword', { code, phone: params.phone });
    else if (params.fromForgotPwd) dispatch(forgotPwd({phone: params.phone, otp: code}));
    else dispatch(changePwd({...params.data, otp: code}));
  }

  return isMounted && (
    <Container style={Style.container}>
      <GoBack />
      <Text
        text={Language.Auth.otp.title}
        weight='bold'
        size='xl'
        color='darkBlue'
        align='center'
        mb={normalize(20)}
      />

      <Text
        text={`${Language.Auth.otp.subTitle} ${params?.phone}`}
        weight='regular'
        size='s'
        color='darkBlueGrey'
        align='center'
        mb={normalize(40)}
      />

      <OTPInputView
        ref={otpRef}
        pinCount={4}
        style={{ height: 100 }}
        codeInputFieldStyle={{
          width: width / 4,
          height: width / 4,
          borderRadius: 10,
          backgroundColor: 'rgba(85, 187, 204, 0.1)',
          borderWidth: 0,
          fontFamily: 'MavenPro-Bold',
          fontSize: normalize(24),
          color: colors.yobenBlue
        }}
        autoFocusOnLoad
        keyboardAppearance='dark'
        placeholderCharacter='-'
        placeholderTextColor={colors.yobenBlue}
        onCodeFilled={onCodeFilled}
      />

      <Text text={alertMsg} color='red' mb={20} mt={20} align='center' />

      <View style={Style.resendContainer}>
        <Text
          text={Language.Auth.otp.resendCode}
          weight='regular'
          size='s'
          color='black'
        />

        {renderResend()}
      </View>

      <Text text={params.otp} />
    </Container>
  )
}

const width = screenWidth - normalize(80);
const Style = StyleSheet.create({
  container: {
    paddingHorizontal: normalize(20),
    paddingTop: normalize(40),
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  inputBox: {
    width: width / 4,
    height: width / 4,
    borderRadius: 10,
    backgroundColor: 'rgba(85, 187, 204, 0.1)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    fontFamily: 'MavenPro-Bold',
    fontSize: normalize(24),
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    color: colors.yobenBlue,
  },
  resendContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 50
  }
})

export default Otp;