import React from 'react';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { normalize } from 'helpers';

const GoBack = () => {
  const navigation = useNavigation();
  function onPressBack() {
    navigation.goBack()
  }

  return (
    <Icon
      name='chevron-left'
      size={normalize(22)}
      color='#D0D0D0'
      style={{ marginBottom: normalize(40) }}
      onPress={onPressBack}
    />
  );
}

export default GoBack;