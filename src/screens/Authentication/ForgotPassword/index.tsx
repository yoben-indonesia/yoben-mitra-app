import React, { useState, useEffect } from 'react';
import { View, StyleSheet, KeyboardAvoidingView, Keyboard, TouchableOpacity, Platform } from 'react-native'
import { useSelector, useDispatch } from 'react-redux';

import { forgotPwd } from 'stores/action';
import { Button, Container, Text, Input } from 'components';
import { isLoading } from 'selectors';
import { navigate, normalize } from 'helpers';
import GoBack from '../goBack';
import Language from 'language';

interface ForgotPasswordProps {
  navigation: any
  route: any
}

const ForgotPassword: React.FC<ForgotPasswordProps> = ({ route: { params } }) => {
  const dispatch = useDispatch();
  const loading = useSelector(isLoading);
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  const [input, setInput] = useState({
    phone: '',
  })

  function handleOnChangeText(value: any, name: any) {
    return setInput({
      ...input,
      [name]: value
    });
  }

  function handleDisabled() {
    return (input.phone)
  }

  function forgotPassword() {
    dispatch(forgotPwd(input));
  }

  return isMounted && (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
      <Container style={Style.container}>
        <GoBack />

        <Text
          text='Lupa Password'
          weight='bold'
          size='xl'
          color='darkBlue'
          mb={normalize(10)}
        />

        <Text
          text='Masukkan nomor telepon Anda.'
          weight='regular'
          size='s'
          color='darkBlueGrey'
          mb={normalize(30)}
        />

        <Input
          placeholder={Language.Auth.login.placeholder1}
          name='phone'
          label='Nomor Telepon'
          value={input.phone}
          mb={30}
          onChangeText={handleOnChangeText}
        />

        <TouchableOpacity activeOpacity={1} style={{ flex: 1 }} onPress={() => Keyboard.dismiss()} />

        <View>
          <Button
            type='large'
            text='Lupa Password'
            disabled={!handleDisabled() || loading}
            loading={loading}
            shadow={true}
            onPress={forgotPassword}
          />
        </View>
      </Container>
    </KeyboardAvoidingView>
  )
}

const Style = StyleSheet.create({
  container: {
    paddingHorizontal: normalize(20),
    paddingVertical: normalize(40),
  }
})

export default ForgotPassword;