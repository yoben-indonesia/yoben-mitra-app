import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, Keyboard, KeyboardAvoidingView, Platform } from "react-native";
import { useDispatch, useSelector } from 'react-redux';

import { login } from 'stores/action';
import { Container, Button, Text, Input } from 'components';
import Language from 'language';
import { navigate, normalize } from 'helpers';
import { isLoading } from 'selectors';

interface LoginProps {
  navigation: any
}

const Login: React.FC<LoginProps> = ({ navigation }) => {
  const dispatch = useDispatch();
  const loading = useSelector(isLoading);
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  const [input, setInput] = useState({
    phone: '',
    password: ''
    // phone: '085156566806', // prod
    // password: 'yoben123'
    // phone: '08234234234', // staging
    // password: 'masuk123',
  })

  function handleOnChangeText(value: any, name: any) {
    return setInput({
      ...input,
      [name]: value
    });
  }

  function onPressLogin() {
    dispatch(login({
      phone: input.phone,
      password: input.password
    }));
  }

  function handleDisabled() {
    return (input.phone && input.password)
  }

  return isMounted && (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
      <Container style={Style.container}>
        <Text
          text={Language.Auth.login.title}
          size='xl'
          color='darkBlue'
          mb={10}
        />
        <Text
          text={Language.Auth.login.subTitle}
          weight='regular'
          size='s'
          color='darkBlueGrey'
          mb={30}
        />
        <Input
          placeholder={Language.Auth.login.placeholder1}
          name='phone'
          label='Nomor Telepon'
          value={input.phone}
          mb={30}
          onChangeText={handleOnChangeText}
        />

        <Input
          placeholder={Language.Auth.login.placeholder2}
          name='password'
          label='Password'
          value={input.password}
          type='password'
          onChangeText={handleOnChangeText}
          mb={10}
        />

        <View style={{ alignItems: 'flex-end' }}>
          <Button type='text' text={Language.Auth.login.button1} onPress={() => navigate('ForgorPassword')}></Button>
        </View>

        <TouchableOpacity activeOpacity={1} style={{ flex: 1 }} onPress={() => Keyboard.dismiss()} />
        <Button
          type='large'
          shadow={true}
          text={Language.Auth.login.button2}
          disabled={!handleDisabled() || loading}
          onPress={onPressLogin}
          activeOpacity={1}
          loading={loading}
        />
      </Container>
    </KeyboardAvoidingView>
  )
}

const Style = StyleSheet.create({
  container: {
    paddingHorizontal: normalize(20),
    paddingVertical: normalize(40),
  }
})

export default Login;