import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Dimensions, TouchableWithoutFeedback } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import { updatePreference } from 'stores/action';
import { Button, Container, Text } from 'components';
import { normalize } from 'helpers';
import { Man, Woman, IluOkSmall } from 'svg';
import { isLoading, userPreference } from 'selectors';
import GoBack from '../goBack';
import { colors } from 'consts';
interface CustPreferencesProps {
  route: any
}

const CustPreferences: React.FC<CustPreferencesProps> = ({ route: { params } }) => {
  const dispatch = useDispatch();
  const loading = useSelector(isLoading);
  const preference = useSelector(userPreference);
  const [male, setMale] = useState(false);
  const [female, setFemale] = useState(false);
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);

    if(!preference) {
      setMale(true);
      setFemale(true);
    } else if (preference === 'm') setMale(true);
    else if (preference === 'f') setFemale(true);
  }, [isMounted]);

  function onPressGender(gender: string) {
    return function () {
      if (gender === 'm') {
        setMale(male ? false : true)
      } else {
        setFemale(female ? false : true)
      }
    }
  }

  function handleDisabled() {
    return !!(male || female)
  }

  function onPressSelect() {
    let preference
    if (male && female) preference = '';
    else if (male) preference = 'm';
    else if (female) preference = 'f';
    dispatch(updatePreference({ user_preference: preference }, params?.fromSetting))
  }

  return isMounted && (
    <Container style={Style(params?.fromSetting).container}>
      <GoBack />

      <Text
        text='Preferensi Customer'
        weight='bold'
        size='xl'
        color='darkBlue'
        mb={20}
      />
      <Text
        text='Pilih preferensi customer Anda.'
        weight='regular'
        size='s'
        color='darkBlueGrey'
        mb={40}
      />

      <View style={Style().genderContainer}>
        <TouchableWithoutFeedback onPress={onPressGender('m')}>
          <View style={male ? Style().activeBox : Style().inactiveBox}>
            <Man></Man>
            {male ? <View style={Style().iluOk}><IluOkSmall /></View> : <></>}
            <Text
              text='Pria'
              weight='bold'
              size='s'
              color='yobenBlue'
              mt={normalize(25)}
            />
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={onPressGender('f')}>
          <View style={female ? Style().activeBox : Style().inactiveBox}>
            <Woman></Woman>
            {female ? <View style={Style().iluOk}><IluOkSmall /></View> : <></>}
            <Text
              text='Perempuan'
              weight='bold'
              size='s'
              color='yobenBlue'
              mt={normalize(25)}
            />
          </View>
        </TouchableWithoutFeedback>
      </View>

      <View style={{ flex: 1 }} />
      <Button type='large' loading={loading} shadow={true} disabled={!handleDisabled()} text='Selesai' onPress={onPressSelect} />
    </Container>
  );
}

const width = Dimensions.get('screen').width - normalize(60);
const Style = (params?: boolean) => StyleSheet.create({
  container: {
    paddingHorizontal: normalize(20),
    paddingBottom: normalize(40),
    paddingTop: params ? normalize(20) : normalize(40),
  },
  genderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  activeBox: {
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 0,
    },
    width: width / 2,
    paddingTop: normalize(40),
    paddingBottom: normalize(20),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: 'white',
    shadowColor: 'rgba(45, 104, 140, 0.1)',
    borderWidth: 2,
    borderColor: colors.yobenBlue,
  },
  inactiveBox: {
    opacity: 0.3,
    borderWidth: 2,
    borderColor: '#F2F2F2',
    width: width / 2,
    paddingTop: normalize(40),
    paddingBottom: normalize(20),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  iluOk: {
    position: 'absolute',
    top: 10,
    right: 10,
  }
})

export default CustPreferences;