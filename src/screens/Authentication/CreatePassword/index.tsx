import React, { useState, useEffect } from 'react';
import { View, StyleSheet, KeyboardAvoidingView, Keyboard, TouchableOpacity, Platform } from 'react-native'
import { useSelector, useDispatch } from 'react-redux';

import { updatePassword, forgotPwd } from 'stores/action';
import { Button, Container, Text, Input } from 'components';
import { navigate, normalize } from 'helpers';
import { isLoading } from 'selectors';
import GoBack from '../goBack';
import Language from 'language';

interface PassManagementProps {
  navigation: any
  route: any
}

const PassManagement: React.FC<PassManagementProps> = ({ route: { params } }) => {
  const dispatch = useDispatch();
  const loading = useSelector(isLoading);
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
  }, [isMounted]);

  const [input, setInput] = useState({
    newPassword: '',
    confirmPassword: '',
  })

  function handleOnChangeText(value: any, name: any) {
    return setInput({
      ...input,
      [name]: value
    });
  }

  function handleDisabled() {
    return (input.newPassword && input.confirmPassword && input.newPassword === input.confirmPassword)
  }

  function submitPassword() {
    if(params.fromForgotPwd) dispatch(forgotPwd({phone: params.phone, password: input.newPassword, otp: params.code, token: params.token}));
    else dispatch(updatePassword({ code: params.code, phone: params.phone, password: input.newPassword }));
  }

  return isMounted && (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
      <Container style={Style.container}>
        <GoBack />

        <Text
          text='Buat Password'
          weight='bold'
          size='xl'
          color='darkBlue'
          mb={normalize(10)}
        />

        <Text
          text='Buat Password baru Anda.'
          weight='regular'
          size='s'
          color='darkBlueGrey'
          mb={normalize(30)}
        />

        <Input
          placeholder='Password Baru'
          name='newPassword'
          label='Password Baru'
          value={input.newPassword}
          type='password'
          onChangeText={handleOnChangeText}
          mb={normalize(20)}
        />

        <Input
          placeholder='Konfirmasi Password Baru'
          name='confirmPassword'
          label='Konfirmasi Kata Sandi Baru'
          value={input.confirmPassword}
          type='password'
          onChangeText={handleOnChangeText}
          mb={normalize(20)}
        />

        <TouchableOpacity activeOpacity={1} style={{ flex: 1 }} onPress={() => Keyboard.dismiss()} />

        <View>
          <Button
            type='large'
            text='Lanjutkan'
            disabled={!handleDisabled() || loading}
            loading={loading}
            shadow={true}
            onPress={submitPassword}
          />
        </View>
      </Container>
    </KeyboardAvoidingView>
  )
}

const Style = StyleSheet.create({
  container: {
    paddingHorizontal: normalize(20),
    paddingVertical: normalize(40),
  }
})

export default PassManagement;