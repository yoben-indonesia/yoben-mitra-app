export default {
  Auth: {
    login: {
      title: 'Masuk',
      subTitle: 'Masukkan nomor telepon & password Anda.',
      placeholder1: 'Nomor Telepon',
      placeholder2: 'Password',
      button1: 'Lupa Password?',
      button2: 'Masuk',
    },
    otp: {
      title: 'Masukkan kode verifikasi',
      subTitle: 'Kami baru saja mengirimkan kode verifikasi ke nomor Anda',
      resendCode: 'Kirim ulang Kode?',
      button: 'Kirim Ulang',
    },
  },

  Home: {
    Home: {
      welcome: 'Selamat Datang!',
      newestOrder: 'Order Tersedia',
      yourOrder: 'Lihat Orderan Anda',
      emptyStateTitle: 'Saldo Anda tidak mencukupi. Top Up sekarang',
      emptyStateDesc:
        'Pastikan saldo Anda minimal Rp 20,000 untuk menerima orderan.',
      emptyStateBtnText: 'Top Up Sekarang',
    },
    Balance: {
      cardTitle: 'Saldo Anda',
      validStatus: 'Anda masih bisa menerima order',
      invalidStatus: 'Top up untuk menerima order',
      listTitle: 'Riwayat Saldo Anda',
      btnText: 'Top Up',
    },
    InfoBalance: {
      title: 'Ketentuan Menerima Orderan',
      titleDesc:
        'Saldo Mitra harus minimal berjumlah Rp 20,000 untuk dapat menerima orderan.',
      minBallance: 'Min. Saldo',
      minAmount: 20000,
      aboutBallanceTitle: 'Aktivitas Sadlo',
      balanceAdd: 'Saldo Bertambah (+)',
      balanceAddDesc:
        'Saldo Mitra akan bertambah apabila Mitra melakukan hal berikut :',
      balanceAdd1:
        '1. Mitra menagih pembayaran ke customer melalui metode pembayaran Bank Transfer',
      balanceAdd2: '2. Mitra melakukan top up (deposit ke pihak YOBEN)',
      balanceReduce: 'Saldo Berkurang (-)',
      balanceReduceDesc:
        'Saldo Mitra akan berkurang apabila Mitra melakukan hal berikut :',
      balanceReduce1:
        '1. Mitra menagih pembayaran ke customer melalui metode pembayaran Tunai / Cash',
    },
    ServiceScore: {
      yourScore: 'Nilai Pelayanan Anda',
      aboutScoreTitle: 'Tentang Nilai Pelayanan',
      verySatisfy: 'Sangat Memuaskan!',
      satisfy: 'Memuaskan',
      sufficient: 'Cukup',
      lessSatisfactory: 'Kurang Memuaskan',
      veryUnsatisfactory: 'Sangat Tidak Memuaskan',
      desc1:
        'Pelayanan dengan kualitas terbaik harus selalu diterapkan oleh semua Mitra Yoben tidak terkecuali Mitra Resik ataupun Mitra Bugar. Raihlah Nilai Pelayanan setinggi mungkin!',
      desc2:
        'Untuk Mitra yang memperoleh nilai penilai rendah dari customer akan mendapatkan sanksi dari pihak YOBEN.',
      starsList: [
        {
          star: '5',
          desc: 'Sangat Memuaskan',
        },
        {
          star: '4',
          desc: 'Memuaskan',
        },
        {
          star: '3',
          desc: 'Cukup',
        },
        {
          star: '2',
          desc: 'Kurang Memuaskan',
        },
        {
          star: '1',
          desc: 'Sangat Tidak Memuaskan',
        },
      ],
    },
  },

  MyOrder: {
    emptyState: 'Mohon bersabar, anda belum ada orderan untuk saat ini.',
    activeEmptyState: 'Update status mitra menjadi AKTIF untuk mendapatkan order!',
  },

  HistoryOrder: {
    today: 'Hari Ini',
    thisWeek: 'Minggu Ini',
    thisMonth: 'Bulan Ini',
    emptyState:
      'Riwayat Order akan muncul saat Anda berhasil menyelesaikan orderan pertama.',
  },

  Payment: {
    paymentMethodTitle: 'Pilih Metode Pembayaran',
    cash: 'Tunai / Cash',
    bank: 'Bank Transfer',
    grandTotal: 'Grand Total',
    serviceFee: 'Biaya Layanan (10%)',
    summary: 'Ringkasan',
    start: 'Mulai',
    finish: 'Selesai',
  },

  MyProfile: {
    orderTotal: 'Total Orderan Bulan Ini',
    favTotal: 'Orang Telah memfavoritkan Anda',
  },

  Setting: {
    tnc: 'Syarat & Ketentuan',
    changePwd: 'Ganti Password',
    custPreferences: 'Preferensi Customer',
    feedback: 'Beri Ulasan',
    logout: 'Keluar',
  },

  TnC: {
    terms: 'Syarat',
    cond: 'Ketentuan',
  },

  Success: {
    TopUp: {
      title: 'Top Up Saldo Akan Diproses',
      desc:
        'Permintaan top up saldo Anda telah berhasil. Permintaan akan diproses oleh admin paling lambat 2x24 jam..',
    },
    Payment: {
      title: 'Orderan Selesai',
      desc:
        'Anda sudah menyelesaikan orderan Anda. Metode pembayaran Bank Transfer akan diproses oleh admin Yoben.',
    },
    btnText: 'Kembali ke Beranda',
  },

  LoremIpsum: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus diam, porttitor at pellentesque non. Dolor ornare feugiat eu nisi fringilla vitae lectus. Vestibulum sapien adipiscing vel lorem. Arcu duis faucibus odio odio turpis feugiat.`,
};
