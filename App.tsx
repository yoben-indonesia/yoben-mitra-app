import React, { useEffect } from 'react';
import { StatusBar } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import CodePush from 'react-native-code-push';
// import firebase from "@react-native-firebase/app";
import moment from 'moment';
import 'moment/min/locales';

import { platformOs } from 'helpers';
import Redux from './src/stores';
import Routes from './src/routers';
import env from './env';

moment.locale('id');
const App = () => {
  useEffect(() => {
    SplashScreen.hide();
    // firebase.initializeApp();
  }, [])

  return (
    <Redux>
      <StatusBar barStyle='dark-content' />
      <Routes />
    </Redux>
  );
};

export default CodePush({
  checkFrequency: CodePush.CheckFrequency.ON_APP_START,
  installMode: CodePush.InstallMode.IMMEDIATE,
  updateDialog: {
    title: 'Update Available',
    mandatoryUpdateMessage: 'An update is available that must be installed.'
  },
  deploymentKey: platformOs(env.kodePushKeyIos, env.kodePushKeyAndroid),
})(App);
